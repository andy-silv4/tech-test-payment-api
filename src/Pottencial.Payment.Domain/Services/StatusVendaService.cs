using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Events;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Domain.Services;

public class StatusVendaService : IStatusVendaService
{
    private readonly IUnitOfWork _unitOfWork;

    public StatusVendaService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<bool> AtualizarStatus(Guid id, StatusVenda statusVenda)
    {
        var venda = await _unitOfWork.VendaRepository.BuscarVenda(id);

        if (venda == null) return false;

        AlterarStatus(statusVenda, venda);

        await _unitOfWork.VendaRepository.AtualizarVenda(venda);

        return await _unitOfWork.Commit();
    }

    private void AlterarStatus(StatusVenda statusVenda, Venda venda)
    {
        var statusAnterior = venda.Status;

        if (statusVenda == StatusVenda.Cancelado)
        {
            venda.CancelarVenda();

            if (statusAnterior == StatusVenda.PagamentoAprovado)
                venda.AdicionaEvento(new EstornoSolicitadoEvent(venda));
        }
        else
            venda.AtualizarStatus(statusVenda);
    }
}
