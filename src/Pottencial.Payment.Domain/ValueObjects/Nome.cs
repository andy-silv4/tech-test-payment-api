namespace Pottencial.Payment.Domain.ValueObjects;

public class Nome : ValueObjects
{
    public string PrimeiroNome { get; private set; }
    public string Sobrenome { get; private set; }

    public Nome(string primeiroNome, string sobrenome)
    {
        PrimeiroNome = primeiroNome;
        Sobrenome = sobrenome;

        EhValido();
    }

    public string NomeCompleto()
    {
        return $"{PrimeiroNome} {Sobrenome}";
    }

    public override bool EhValido()
    {
        if (string.IsNullOrEmpty(PrimeiroNome.Trim()) ||
            string.IsNullOrEmpty(Sobrenome.Trim()))
            Erros.Add("O nome não poder ser vazio ou conter apenas espaços em branco");

        return !Erros.Any();
    }
}
