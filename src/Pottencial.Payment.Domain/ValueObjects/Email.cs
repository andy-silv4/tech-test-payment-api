using System.Net.Mail;

namespace Pottencial.Payment.Domain.ValueObjects;

public class Email : ValueObjects
{
    public string Endereco { get; private set; }

    public Email(string endereco)
    {
        Endereco = endereco;

        EhValido();
    }

    public override bool EhValido()
    {
        if (!MailAddress.TryCreate(Endereco, out var _))
            Erros.Add("O endereco de email precisa ser valido");

        return !Erros.Any();
    }
}
