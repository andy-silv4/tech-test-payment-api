namespace Pottencial.Payment.Domain.ValueObjects;

public class Telefone : ValueObjects
{
    public const int TamanhoDdd = 2;
    public const int TamanhoMinimo = 8;
    public const int TamanhoMaximo = 9;

    public string Ddd { get; private set; }
    public string Numero { get; private set; }

    public Telefone(string ddd, string numero)
    {
        Ddd = ddd;
        Numero = numero;

        EhValido();
    }

    public string TelefoneCompleto()
    {
        var parse = long.Parse($"{Ddd}{Numero}");

        if (Numero.Count() == TamanhoMinimo)
            return parse.ToString(@"(00) 0000-0000");

        return parse.ToString(@"(00) 00000-0000");
    }

    public override bool EhValido()
    {
        if (Ddd.Length != TamanhoDdd)
            Erros.Add($"O ddd deve ter {TamanhoDdd} digitos");

        if (Numero.Length < TamanhoMinimo || Numero.Length > TamanhoMaximo)
            Erros.Add($"O telefone deve ter entre {TamanhoMinimo} e {TamanhoMaximo} digitos");

        if (!(int.TryParse(Ddd, out _)) || !(long.TryParse(Numero, out _)))
            Erros.Add($"O telefone deve possuir apenas numeros");

        return !Erros.Any();
    }
}
