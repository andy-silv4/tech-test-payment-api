using System.ComponentModel.DataAnnotations.Schema;

namespace Pottencial.Payment.Domain.ValueObjects;

public abstract class ValueObjects
{
    public List<string> Erros { get; protected set; }

    protected ValueObjects()
    {
        Erros = new List<string>();
    }

    public abstract bool EhValido();
}
