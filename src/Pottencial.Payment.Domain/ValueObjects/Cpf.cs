namespace Pottencial.Payment.Domain.ValueObjects;

public class Cpf : ValueObjects
{
    private const int Digitos = 11;

    public string Numero { get; private set; }

    public Cpf(string numero)
    {
        Numero = numero;

        EhValido();
    }

    public string CpfFormatado()
    {
        return Convert.ToInt64(Numero).ToString(@"###\.###\.###\-00");
    }

    public string CpfMascarado()
    {
        return CpfFormatado().Remove(4, 7).Insert(4, "***.***");
    }

    public override bool EhValido()
    {
        if (Numero.Length != Digitos || !(long.TryParse(Numero, out _))) //ValidarCpf()
            Erros.Add($"O cpf deve possuir {Digitos} digitos e ser valido");

        return !Erros.Any();
    }
}
