namespace Pottencial.Payment.Domain.Enums;

public enum StatusVenda
{
    AguardandoPagamento,
    PagamentoAprovado,
    EnviadoParaTransportadora,
    Entregue,
    Cancelado
}
