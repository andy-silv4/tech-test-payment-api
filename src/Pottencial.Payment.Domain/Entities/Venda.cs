using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Events;
using Pottencial.Payment.Domain.Exceptions;

namespace Pottencial.Payment.Domain.Entities;

public class Venda : Entity
{
    private readonly List<VendaItem> _itens = new List<VendaItem>();

    public DateTime DataVenda { get; private set; }
    public StatusVenda Status { get; private set; }
    public decimal ValorTotal { get; private set; }
    public IReadOnlyCollection<VendaItem> Itens => _itens;

    public Guid VendedorId { get; private set; }
    public Vendedor? Vendedor { get; private set; }

    private Venda() { }

    public Venda(Guid vendedorId)
    {
        DataVenda = DateTime.Now;
        VendedorId = vendedorId;

        if (!EhValido()) throw new DomainException(string.Join("|", Erros));

        Status = StatusVenda.AguardandoPagamento;
    }

    public void AtualizarStatus(StatusVenda statusDestino)
    {
        var statusNecessario = statusDestino switch
        {
            StatusVenda.Entregue => StatusVenda.EnviadoParaTransportadora,
            StatusVenda.EnviadoParaTransportadora => StatusVenda.PagamentoAprovado,
            StatusVenda.PagamentoAprovado => StatusVenda.AguardandoPagamento,
            _ => StatusVenda.Cancelado
        };

        if (Status != statusNecessario)
            throw new DomainException($"Não é possível alterar o status de '{Status}' para '{statusDestino}'");

        Status = statusDestino;

        AdicionaEvento(new StatusAtualizadoEvent(this));
    }

    public void CancelarVenda()
    {
        Status = Status switch
        {
            StatusVenda.AguardandoPagamento => StatusVenda.Cancelado,
            StatusVenda.PagamentoAprovado => StatusVenda.Cancelado,
            _ => throw new DomainException($"Não é possível cancelar a venda com o status '{Status}'")
        };

        AdicionaEvento(new StatusAtualizadoEvent(this));
    }

    public Venda AdicionarItens(IEnumerable<VendaItem> itens)
    {
        foreach (var item in itens)
        {
            if (item.VendaId != Id)
                throw new DomainException($"O item deve ter o mesmo identificador da venda");

            _itens.Add(item);
        }

        return this;
    }

    public void CalcularValorTotal()
    {
        if (Itens == null || Itens.Count < 1)
            throw new DomainException($"A venda deve ter no minimo 1 item");

        ValorTotal = Itens.Sum(x => x.ValorTotal());
    }

    public override bool EhValido()
    {
        if (VendedorId == default(Guid) || VendedorId == Guid.Empty)
            Erros.Add($"A venda deve ter um identificador valido para o '{nameof(VendedorId)}'");

        return !Erros.Any();
    }
}
