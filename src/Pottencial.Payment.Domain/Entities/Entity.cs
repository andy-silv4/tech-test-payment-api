using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Domain.Entities;

public abstract class Entity
{
    private List<IEvent> _eventos = new List<IEvent>();

    public Guid Id { get; protected set; }
    public List<string> Erros { get; protected set; }
    public IReadOnlyCollection<IEvent> Eventos => _eventos.AsReadOnly();

    protected Entity()
    {
        Id = Guid.NewGuid();
        Erros = new List<string>();
    }

    public void AdicionaEvento(IEvent evento) => _eventos.Add(evento);

    public void RemoveEventos() => _eventos.Clear();

    public abstract bool EhValido();
}
