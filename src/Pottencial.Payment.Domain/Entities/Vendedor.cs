using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.ValueObjects;

namespace Pottencial.Payment.Domain.Entities;

public class Vendedor : Entity
{
    public Nome Nome { get; private set; } = null!;
    public Cpf Cpf { get; private set; } = null!;
    public Email Email { get; private set; } = null!;
    public Telefone Telefone { get; private set; } = null!;

    private Vendedor() { }

    public Vendedor(Nome nome, Cpf cpf, Email email, Telefone telefone)
    {
        Nome = nome;
        Cpf = cpf;
        Email = email;
        Telefone = telefone;

        if (!EhValido()) throw new DomainException(string.Join("|", Erros));
    }

    public override bool EhValido()
    {
        Erros.AddRange(Nome.Erros);
        Erros.AddRange(Cpf.Erros);
        Erros.AddRange(Email.Erros);
        Erros.AddRange(Telefone.Erros);

        return !Erros.Any();
    }
}
