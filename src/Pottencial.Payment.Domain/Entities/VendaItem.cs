using Pottencial.Payment.Domain.Exceptions;

namespace Pottencial.Payment.Domain.Entities;

public class VendaItem : Entity
{
    public const int QuantidadeMinima = 1;
    public const decimal ValorMinimo = 0.01M;


    public int OrdemItem { get; set; }
    public string ProdutoId { get; private set; } = null!;
    public string NomeProduto { get; private set; } = null!;
    public int Quantidade { get; private set; }
    public decimal ValorUnidade { get; private set; }

    public Guid VendaId { get; private set; }
    public Venda? Venda { get; private set; }

    private VendaItem() { }

    public VendaItem(Guid vendaId, string produtoId, int ordemItem,
                     string descricao, int quantidade,
                     decimal valorUnidade)
    {
        VendaId = vendaId;
        OrdemItem = ordemItem;
        ProdutoId = produtoId;
        NomeProduto = descricao;
        Quantidade = quantidade;
        ValorUnidade = valorUnidade;

        if (!EhValido()) throw new DomainException(string.Join("|", Erros));
    }

    public decimal ValorTotal()
    {
        return Quantidade * ValorUnidade;
    }

    public override bool EhValido()
    {
        if (VendaId == Guid.Empty || VendaId == default(Guid))
            Erros.Add($"O item deve ter um identificador de uma venda existente");

        if (string.IsNullOrEmpty(ProdutoId))
            Erros.Add($"O item deve ter o identificador do produto");

        if (string.IsNullOrEmpty(NomeProduto))
            Erros.Add($"O item deve ter o nome do produto");

        if (Quantidade < QuantidadeMinima)
            Erros.Add($"A quantidade do produto deve ser no minimo {QuantidadeMinima}");

        if (ValorUnidade < ValorMinimo)
            Erros.Add($"O valor do produto deve ser no minimo {ValorMinimo}");

        return !Erros.Any();
    }
}
