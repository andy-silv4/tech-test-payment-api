namespace Pottencial.Payment.Domain.Entities;

public class Evento
{
    public Guid Id { get; private set; }
    public Guid EventoId { get; private set; }
    public string Mensagem { get; private set; } = null!;
    public DateTime SalvoEm { get; private set; }

    public Evento(Guid eventoId, string mensagem)
    {
        Id = Guid.NewGuid();
        EventoId = eventoId;
        Mensagem = mensagem;
        SalvoEm = DateTime.Now;
    }

    private Evento() { }
}
