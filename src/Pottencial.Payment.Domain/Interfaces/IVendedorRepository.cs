using System.Linq.Expressions;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.ValueObjects;

namespace Pottencial.Payment.Domain.Interfaces;

public interface IVendedorRepository : IDisposable
{
    Task<Vendedor> BuscarVendedor(Expression<Func<Vendedor, bool>> query);
    Task<Vendedor> BuscarVendedor(Cpf cpf);
    Task AdicionarVendedor(Vendedor vendedor);
    Task<bool> ExisteVendedor(Vendedor vendedor);
    Task<bool> ExisteVendedor(Guid id);
}
