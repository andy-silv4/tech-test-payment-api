namespace Pottencial.Payment.Domain.Interfaces;

public interface IEventDispatcher
{
    Task Publish<TEvent>(TEvent message)
        where TEvent : IEvent;
}

public interface IEventDispatcherRegister
{
    void Register<TEvent>(IEventHandler<TEvent> handler)
        where TEvent : IEvent;
    void Unregister<TRequest>()
        where TRequest : IEvent;
}
