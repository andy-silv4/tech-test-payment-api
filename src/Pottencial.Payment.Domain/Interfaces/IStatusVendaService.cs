using Pottencial.Payment.Domain.Enums;

namespace Pottencial.Payment.Domain.Interfaces;

public interface IStatusVendaService
{
    Task<bool> AtualizarStatus(Guid id, StatusVenda statusPagamento);
}
