namespace Pottencial.Payment.Domain.Interfaces;

public interface IUnitOfWork
{
    public IVendaRepository VendaRepository { get; }
    public IVendedorRepository VendedorRepository { get; }

    Task<bool> Commit();
}
