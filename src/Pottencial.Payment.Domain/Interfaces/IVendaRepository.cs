using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Domain.Interfaces;

public interface IVendaRepository : IDisposable
{
    Task<Venda> BuscarVenda(Guid id);
    Task<Venda> BuscarVendaComInformacoes(Guid id);
    Task AdicionarVenda(Venda venda);
    Task AtualizarVenda(Venda venda);
    Task Commit();
}
