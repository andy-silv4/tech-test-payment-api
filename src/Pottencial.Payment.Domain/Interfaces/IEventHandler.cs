namespace Pottencial.Payment.Domain.Interfaces;

public interface IEventHandler { }

public interface IEventHandler<TEvent> : IEventHandler
    where TEvent : IEvent
{
    Task Handle(TEvent evento);
}

public interface IDecoratorEventHandler
{
    public Task Handle(IEvent evento);
}
