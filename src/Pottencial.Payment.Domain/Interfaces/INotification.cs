namespace Pottencial.Payment.Domain.Interfaces;

public interface INotificador
{
    IReadOnlyCollection<string> Notificacoes { get; }
    bool ExistemNotificacoes { get; }

    void AdicionarNotificacao(string message);
}

public class Notificador : INotificador
{
    private readonly List<string> _notificacoes;
    public IReadOnlyCollection<string> Notificacoes => _notificacoes;
    public bool ExistemNotificacoes => _notificacoes.Any();

    public Notificador()
    {
        _notificacoes = new List<string>();
    }

    public void AdicionarNotificacao(string mensagem)
    {
        _notificacoes.AddRange(mensagem.Split("|").ToList());
    }
}
