using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Domain.Events;

public class VendaRegistradaEvent : IEvent
{
    public Venda Venda { get; private set; }
    public DateTime CriadoEm { get; private set; }

    public VendaRegistradaEvent(Venda venda)
    {
        Venda = venda;
        CriadoEm = DateTime.Now;
    }
}
