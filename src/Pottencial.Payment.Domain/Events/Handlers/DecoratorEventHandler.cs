using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Domain.Events.Handlers;

public class DecoratorEventHandler<TEvent> : IDecoratorEventHandler
    where TEvent : IEvent
{
    private readonly IEventHandler<TEvent> _eventHandler;

    public DecoratorEventHandler(IEventHandler<TEvent> eventHandler)
    {
        _eventHandler = eventHandler;
    }

    public async Task Handle(IEvent evento)
    {
        var eventHandler = (IEventHandler<TEvent>)_eventHandler;

        await eventHandler.Handle((TEvent)evento);
    }
}
