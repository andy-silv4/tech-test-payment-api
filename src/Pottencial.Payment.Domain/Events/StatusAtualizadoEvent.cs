using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Domain.Events;

public class StatusAtualizadoEvent : IEvent
{
    public Guid Id { get; private set; }
    public StatusVenda StatusVenda { get; private set; }
    public DateTime CriadoEm { get; private set; }

    public StatusAtualizadoEvent(Venda venda)
    {
        Id = venda.Id;
        StatusVenda = venda.Status;
        CriadoEm = DateTime.Now;
    }
}
