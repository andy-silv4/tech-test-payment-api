using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Domain.Events;

public class EstornoSolicitadoEvent : IEvent
{
    public Guid Id { get; private set; }
    public decimal ValorTotal { get; private set; }
    public DateTime CriadoEm { get; private set; }

    public EstornoSolicitadoEvent(Venda venda)
    {
        Id = venda.Id;
        ValorTotal = venda.ValorTotal;
        CriadoEm = DateTime.Now;
    }
}
