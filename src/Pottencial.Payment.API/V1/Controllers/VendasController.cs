using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Application.V1.Services;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.API.V1.Controllers;

[ApiController]
[ApiVersion("1.0", Deprecated = false)]
[Route("api/v{version:apiVersion}/vendas")]
[Produces("application/json")]
public class VendasController : ControllerBase
{
    private readonly INotificador _notificador;
    private readonly IVendaService _service;

    public VendasController(INotificador notificador,
                            IVendaService service)
    {
        _notificador = notificador;
        _service = service;
    }

    /// <summary>
    /// Busca uma venda existente pelo identificador
    /// </summary>
    /// <param name="id">Identificador de uma venda existente</param>
    /// <returns>Uma venda</returns>
    /// <response code="200">Retorna a venda</response>
    /// <response code="404">Identificador nao tem venda registrada</response>
    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult<VendaResponse>> BuscarVenda(Guid id)
    {
        var response = await _service.BuscarVenda(id);

        if (response == null) return NotFound();

        return Ok(response);
    }

    /// <summary>
    /// Registra uma nova venda com os dados do vendedor e os itens vendidos
    /// </summary>
    /// <param name="request"></param>
    /// <returns>Uma venda</returns>
    /// <response code="201">Retorna a venda criada</response>
    /// <response code="400">Retorna erros de validação ou criação</response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ErroResponse), StatusCodes.Status400BadRequest)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult<VendaResponse>> RegistrarVenda([FromBody] VendaRequest request)
    {
        var response = await _service.AdicionarVenda(request);

        if (_notificador.ExistemNotificacoes)
            return BadRequest(new { Erros = _notificador.Notificacoes });

        return CreatedAtAction(nameof(BuscarVenda), new { Id = response.Id }, response);
    }

    /// <summary>
    /// Atualiza o status de uma venda
    /// </summary>
    /// <param name="id">Identificador de uma venda existente</param>
    /// <param name="request">Status para atualização</param>
    /// <returns></returns>
    /// <response code="204">Atualizado com sucesso</response>
    /// <response code="400">Retorna erros de validação ou criação</response>
    /// <response code="404">Identificador nao tem venda registrada</response>
    [HttpPatch("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErroResponse), StatusCodes.Status400BadRequest)]
    [ProducesDefaultResponseType]
    public async Task<IActionResult> AtualizarStatusVenda(Guid id, StatusRequest request)
    {
        var atualizado = await _service.AtualizarStatusVenda(id, request.VendaStatus);

        if (_notificador.ExistemNotificacoes)
            return BadRequest(new ErroResponse(_notificador.Notificacoes.ToList()));

        if (!atualizado) return NotFound();

        return NoContent();
    }
}
