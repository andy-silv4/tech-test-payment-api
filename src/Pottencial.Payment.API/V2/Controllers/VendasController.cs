using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.V2.DTOs;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.BuscarVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.API.V2.Controllers;

[ApiController]
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/vendas")]
[Produces("application/json")]
public class VendasController : ControllerBase
{
    private readonly ILogger<VendasController> _logger;
    private readonly INotificador _notificador;
    private readonly IMediator _mediator;

    public VendasController(ILogger<VendasController> logger,
                            INotificador notificador,
                            IMediator mediator)
    {
        _logger = logger;
        _notificador = notificador;
        _mediator = mediator;
    }

    /// <summary>
    /// Busca uma venda existente pelo identificador
    /// </summary>
    /// <param name="id">Identificador de uma venda existente</param>
    /// <returns>Uma venda</returns>
    /// <response code="200">Retorna a venda</response>
    /// <response code="404">Identificador nao tem venda registrada</response>
    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult<VendaQueryResult>> BuscarVenda(Guid id)
    {
        var query = new BuscarVendaPorIdQuery(id);
        var result = await _mediator.Send<BuscarVendaPorIdQuery, VendaQueryResult>(query);

        if (result.Id == Guid.Empty) return NotFound();

        return Ok(result);
    }

    /// <summary>
    /// Registra uma nova venda com os itens vendidos
    /// </summary>
    /// <param name="command"></param>
    /// <returns>Uma venda</returns>
    /// <response code="201">Retorna a venda criada</response>
    /// <response code="400">Retorna erros de validação ou criação</response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ErroResult), StatusCodes.Status400BadRequest)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult<VendaCommandResult>> RegistrarVenda([FromBody] RegistrarVendaCommand command)
    {
        var result = await _mediator.Send<RegistrarVendaCommand, VendaCommandResult>(command);

        if (_notificador.ExistemNotificacoes)
            return BadRequest(new { Erros = _notificador.Notificacoes });

        return CreatedAtAction(nameof(BuscarVenda), new { Id = result.Id }, result);
    }

    /// <summary>
    /// Atualiza o status de uma venda
    /// </summary>
    /// <param name="id">Identificador de uma venda existente</param>
    /// <param name="command">Status para atualização</param>
    /// <returns></returns>
    /// <response code="204">Atualizado com sucesso</response>
    /// <response code="400">Retorna erros de validação ou criação</response>
    /// <response code="404">Identificador nao tem venda registrada</response>
    [HttpPatch("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(ErroResult), StatusCodes.Status400BadRequest)]
    [ProducesDefaultResponseType]
    public async Task<IActionResult> AtualizarStatusVenda(Guid id, AtualizarStatusVendaCommand command)
    {
        command.Id = id;
        var result = await _mediator.Send<AtualizarStatusVendaCommand, StatusVendaCommandResult>(command);

        if (_notificador.ExistemNotificacoes)
            return BadRequest(new ErroResult(_notificador.Notificacoes.ToList()));

        if (!result.Atualizado) return NotFound();

        return NoContent();
    }
}
