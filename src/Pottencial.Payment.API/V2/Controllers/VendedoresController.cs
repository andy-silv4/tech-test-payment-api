using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.V2.DTOs;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;
using Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.API.V2.Controllers;

[ApiController]
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/vendedores")]
[Produces("application/json")]
public class VendedoresController : ControllerBase
{
    private readonly INotificador _notificador;
    private readonly IMediator _mediator;

    public VendedoresController(INotificador notificador,
                                IMediator mediator)
    {
        _notificador = notificador;
        _mediator = mediator;
    }

    /// <summary>
    /// Busca uma vendedor existente pelo identificador
    /// </summary>
    /// <param name="id">Identificador de um vendedor existente</param>
    /// <returns>Um vendedor</returns>
    /// <response code="200">Retorna o vendedor</response>
    /// <response code="404">Identificador nao tem vendededor cadastrado</response>
    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult<VendedorQueryResult>> BuscarVendedor(Guid id)
    {
        var query = new BuscarVendedorPorIdQuery(id);
        var result = await _mediator.Send<BuscarVendedorPorIdQuery, VendedorQueryResult>(query);

        if (result.Id == Guid.Empty) return NotFound();

        return Ok(result);
    }

    /// <summary>
    /// Busca uma vendedor existente pelo e-mail
    /// </summary>
    /// <param name="email">E-mail de um vendedor existente</param>
    /// <returns>Um vendedor</returns>
    /// <response code="200">Retorna o vendedor</response>
    /// <response code="404">E-mail nao tem vendededor cadastrado</response>
    [HttpGet("{email}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult<VendedorQueryResult>> BuscarVendedor(string email)
    {
        var query = new BuscarVendedorPorEmailQuery(email);
        var result = await _mediator.Send<BuscarVendedorPorEmailQuery, VendedorQueryResult>(query);

        if (result.Id == Guid.Empty) return NotFound();

        return Ok(result);
    }

    /// <summary>
    /// Cadastra um novo vendedor
    /// </summary>
    /// <param name="command"></param>
    /// <returns>Um vendedor</returns>
    /// <response code="201">Retorna o vendedor cadastrado</response>
    /// <response code="400">Retorna erros de validacao ou criacao</response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ErroResult), StatusCodes.Status400BadRequest)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult<VendedorCommandResult>> CadastrarVendedor([FromBody] CadastrarVendedorCommand command)
    {
        var result = await _mediator.Send<CadastrarVendedorCommand, VendedorCommandResult>(command);

        if (_notificador.ExistemNotificacoes)
            return BadRequest(new ErroResult(_notificador.Notificacoes));

        return CreatedAtAction(nameof(BuscarVendedor), new { Id = result.Id }, result);
    }
}
