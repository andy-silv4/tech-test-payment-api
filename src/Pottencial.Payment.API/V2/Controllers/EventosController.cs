using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.API.V2.Controllers;

[ApiController]
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}")]
[Produces("application/json")]
[ApiExplorerSettings(IgnoreApi = true)]
public class EventosController : ControllerBase
{
    private readonly ApplicationDbContext _context;

    public EventosController(ApplicationDbContext context)
    {
        _context = context;
    }

    [HttpGet("vendas/eventos")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult> ExibirEventos()
    {
        var result = await _context.Eventos
                        .AsNoTracking()
                        .OrderByDescending(x => x.SalvoEm)
                        .Take(15)
                        .ToListAsync();

        return Ok(result);
    }

    [HttpGet("vendas/{id:guid}/eventos")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult> BuscarEventosPorId(Guid id)
    {
        var result = await _context.Eventos
                        .AsNoTracking()
                        .Where(x => x.EventoId == id)
                        .OrderByDescending(x => x.SalvoEm)
                        .ToListAsync();

        if (!result.Any()) return NotFound();

        return Ok(result);
    }
}
