using System.Net;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.V1.DTOs;

namespace Pottencial.Payment.API.Middleware;

public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;
    private readonly IHostEnvironment _hostEnvironment;
    private readonly ILogger<ExceptionMiddleware> _logger;

    public ExceptionMiddleware(RequestDelegate next,
                               [FromServices] IHostEnvironment hostEnvironment,
                               [FromServices] ILogger<ExceptionMiddleware> logger)
    {
        _next = next;
        _hostEnvironment = hostEnvironment;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, exception.Message);

            await HandleExceptionAsync(httpContext, exception);
        }
    }

    private async Task HandleExceptionAsync(HttpContext context, Exception exception)
    {
        ErroResponse response;

        if (_hostEnvironment.IsDevelopment())
            response = new ErroResponse($"{exception.Message} {exception?.InnerException?.Message}");
        else
            response = new ErroResponse("Ocorreu um erro interno do servidor");

        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

        await context.Response.WriteAsJsonAsync<ErroResponse>(response);
    }
}
