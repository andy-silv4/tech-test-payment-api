using Microsoft.AspNetCore.HttpLogging;

namespace Pottencial.Payment.API.Configurations;

public static class Logger
{
    public static IServiceCollection AddSeqLogging(this IServiceCollection services, IConfiguration configuration)
    {
        var seqConfig = configuration.GetSection("Seq");

        seqConfig["ServerUrl"] = Environment.GetEnvironmentVariable("SERVER_URL");
        seqConfig["ApiKey"] = Environment.GetEnvironmentVariable("API_KEY");

        services.AddHttpLogging(logging =>
        {
            logging.LoggingFields = HttpLoggingFields.RequestPropertiesAndHeaders | HttpLoggingFields.ResponsePropertiesAndHeaders;

        }).AddLogging(loggingBuilder =>
        {
            loggingBuilder.AddSeq(seqConfig);
        });

        return services;
    }
}
