using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pottencial.Payment.API.Configurations.Swagger;

public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
{
    private readonly IApiVersionDescriptionProvider _provider;

    public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider) => _provider = provider;

    public void Configure(SwaggerGenOptions options)
    {
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
        }
    }

    static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
    {
        var info = new OpenApiInfo
        {
            Version = description.ApiVersion.ToString(),
            Title = "Payment API",
            Description = "API REST desenvolvida em .NET 6 para registro de vendas",
            TermsOfService = new Uri("https://gitlab.com/Pottencial/tech-test-payment-api/-/blob/main/README.md"),
            Contact = new OpenApiContact
            {
                Name = "Pottencial",
                Url = new Uri("https://pottencial.com.br")
            }
        };

        if (description.IsDeprecated)
            info.Description += "Esta versão da API foi descontinuada.";

        return info;
    }
}
