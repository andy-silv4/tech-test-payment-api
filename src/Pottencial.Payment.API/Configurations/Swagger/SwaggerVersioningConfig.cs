using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pottencial.Payment.API.Configurations.Swagger;

public static class SwaggerVersioningConfig
{
    public static IServiceCollection AddSwaggerVersioningGen(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSwaggerApiVersioning(configuration);

        services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
        services.AddSwaggerGen(options =>
        {
            options.OperationFilter<SwaggerDefaultValues>();

            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
        });

        return services;
    }

    public static IServiceCollection AddSwaggerApiVersioning(this IServiceCollection services, IConfiguration configuration)
    {
        var apiVersion = configuration.GetRequiredSection("ApiVersion").Value;
        services.AddApiVersioning(options =>
        {
            options.ReportApiVersions = true;
            options.AssumeDefaultVersionWhenUnspecified = true;

            options.DefaultApiVersion = ApiVersion.Parse(apiVersion);
        });

        services.AddVersionedApiExplorer(options =>
        {
            options.GroupNameFormat = "'v'VVV";
            options.SubstituteApiVersionInUrl = true;
        });

        services.AddEndpointsApiExplorer();

        return services;
    }

    public static IApplicationBuilder UseSwaggerVersioning(this IApplicationBuilder app,
                                                           IApiVersionDescriptionProvider provider)
    {
        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            var sortedByLatestVersion = provider.ApiVersionDescriptions.Reverse();
            foreach (var description in sortedByLatestVersion)
            {
                options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                    description.GroupName.ToUpperInvariant());
            }
        });

        return app;
    }
}
