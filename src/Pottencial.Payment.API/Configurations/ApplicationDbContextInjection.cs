using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.API.Configurations;

public static class ApplicationDbContextInjection
{
    public static IServiceCollection AddApplicationDbContext(this IServiceCollection services,
                                                             IHostEnvironment environment,
                                                             IConfiguration configuration)
    {
        if (environment.IsDevelopment())
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                            options.UseInMemoryDatabase("PaymentDb"));
        }
        else
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(configuration.GetConnectionString("PaymentDb")));
        }

        return services;
    }
}
