using Pottencial.Payment.API.Application.Services;
using Pottencial.Payment.Application.V1.Services;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Domain.Services;
using Pottencial.Payment.Infra.Context;
using Pottencial.Payment.Infra.Repositories;

namespace Pottencial.Payment.API.Configurations;

public static class DependencyInjection
{
    public static IServiceCollection AddDependencyInjection(this IServiceCollection services)
    {
        services.AddScoped<IUnitOfWork, UnitOfWork>();

        services.AddScoped<IVendaRepository, VendaRepository>();
        services.AddScoped<IVendedorRepository, VendedorRepository>();

        services.AddScoped<IVendaService, VendaService>();
        services.AddScoped<IVendedorService, VendedorService>();
        services.AddScoped<IStatusVendaService, StatusVendaService>();

        services.AddScoped<INotificador, Notificador>();

        return services;
    }
}