using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Application.V2.Services;
using Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda.Events;
using Pottencial.Payment.Application.V2.UseCases.Vendas.BuscarVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda.Events;
using Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;
using Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;
using Pottencial.Payment.Domain.Events;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.API.Configurations;

public static class MediatorDependencyInjection
{
    public static IServiceCollection AddMediator(this IServiceCollection services)
    {
        services.AddScoped<IRequestHandler<CadastrarVendedorCommand, VendedorCommandResult>, CadastrarVendedorHandler>();
        services.AddScoped<IRequestHandler<RegistrarVendaCommand, VendaCommandResult>, RegistrarVendaHandler>();
        services.AddScoped<IRequestHandler<AtualizarStatusVendaCommand, StatusVendaCommandResult>, AtualizarStatusVendaHandler>();

        services.AddScoped<IRequestHandler<BuscarVendedorPorIdQuery, VendedorQueryResult>, BuscarVendedorHandler>();
        services.AddScoped<IRequestHandler<BuscarVendedorPorEmailQuery, VendedorQueryResult>, BuscarVendedorHandler>();
        services.AddScoped<IRequestHandler<BuscarVendaPorIdQuery, VendaQueryResult>, BuscarVendaHandler>();

        services.AddScoped<IMediator, Mediator>();

        return services;
    }

    public static IServiceCollection AddEventDispatcher(this IServiceCollection services)
    {
        services.AddScoped<IEventHandler<VendaRegistradaEvent>, VendaRegistradaHandler>();
        services.AddScoped<IEventHandler<StatusAtualizadoEvent>, StatusAtualizadoHandler>();
        services.AddScoped<IEventHandler<EstornoSolicitadoEvent>, EstornoSolicitadoHandler>();

        services.AddScoped<EventDispatcher>();
        services.AddScoped<IEventDispatcher>(serviceProvider =>
        {
            var dispatcher = serviceProvider.GetRequiredService<EventDispatcher>();

            dispatcher.Register<StatusAtualizadoEvent>(serviceProvider.GetRequiredService<IEventHandler<StatusAtualizadoEvent>>());
            dispatcher.Register<VendaRegistradaEvent>(serviceProvider.GetRequiredService<IEventHandler<VendaRegistradaEvent>>());
            dispatcher.Register<EstornoSolicitadoEvent>(serviceProvider.GetRequiredService<IEventHandler<EstornoSolicitadoEvent>>());

            return dispatcher;
        });

        return services;
    }
}
