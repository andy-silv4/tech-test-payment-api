using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Pottencial.Payment.API.Configurations;
using Pottencial.Payment.API.Configurations.Swagger;
using Pottencial.Payment.API.Middleware;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSeqLogging(builder.Configuration);

builder.Services.AddControllers()
    .AddJsonOptions(options =>
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

builder.Services.AddApplicationDbContext(builder.Environment, builder.Configuration);

builder.Services.AddDependencyInjection();

builder.Services.AddMediator();
builder.Services.AddEventDispatcher();

builder.Services.AddSwaggerVersioningGen(builder.Configuration);

var app = builder.Build();

app.UseHttpLogging();

if (app.Environment.IsDevelopment())
{
    var apiVersionDescriptionProvider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();
    app.UseSwaggerVersioning(apiVersionDescriptionProvider);
}

app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

public partial class Program { }
