using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.ValueObjects;

namespace Pottencial.Payment.API.Application.V1.Mappers;

public static class VendedorMapper
{
    public static Vendedor ParaEntity(this VendedorRequest input)
    {
        if (input == null) return (Vendedor)null!;

        return new Vendedor(new Nome(input.Nome!, input.Sobrenome!),
                            new Cpf(input.Cpf),
                            new Email(input.Email!),
                            new Telefone(input.Ddd!, input.Telefone!));
    }

    public static VendedorResponse ParaOutput(this Vendedor vendedor)
    {
        if (vendedor == null) return (VendedorResponse)null!;

        return new VendedorResponse(vendedor!.Id, vendedor.Cpf.CpfMascarado(),
                                    vendedor.Nome.NomeCompleto(), vendedor.Email.Endereco,
                                    vendedor.Telefone.TelefoneCompleto());
    }
}
