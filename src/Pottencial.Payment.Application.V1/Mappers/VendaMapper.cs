using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.API.Application.V1.Mappers;

public static class VendaMapper
{
    public static VendaResponse ParaOutput(this Venda venda)
    {
        if (venda == null) return (VendaResponse)null!;

        var itensResponse = venda.Itens.ParaOutput();

        var vendedorResponse = venda.Vendedor!.ParaOutput();

        return new VendaResponse(venda.Id, venda.DataVenda,
                                              venda.Status.ToString(),
                                              vendedorResponse,
                                              itensResponse, venda.ValorTotal);
    }
}
