using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.API.Application.V1.Mappers;

public static class ItemVendaMapper
{
    public static IEnumerable<VendaItem> ParaEntity(this IEnumerable<ItemRequest> input, Guid vendaId)
    {
        if (input == null) return (IList<VendaItem>)null!;

        int ordem = 1;
        return input.Select(x =>
            new VendaItem(vendaId, x.CodigoProduto, ordem++,
                          x.NomeProduto, x.Quantidade, x.ValorUnidade)
        ).ToList();
    }

    public static IEnumerable<ItemResponse> ParaOutput(this IEnumerable<VendaItem> itens)
    {
        if (itens == null) return (IList<ItemResponse>)null!;

        return itens.Select(x =>
            new ItemResponse(x.ProdutoId, x.NomeProduto, x.Quantidade, x.ValorUnidade, x.ValorTotal())
        ).ToList();
    }
}
