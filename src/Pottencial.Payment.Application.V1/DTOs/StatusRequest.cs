using System.Text.Json.Serialization;

namespace Pottencial.Payment.Application.V1.DTOs;

public class StatusRequest
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public VendaStatus VendaStatus { get; set; }

    public StatusRequest(VendaStatus vendaStatus)
    {
        VendaStatus = vendaStatus;
    }
}
