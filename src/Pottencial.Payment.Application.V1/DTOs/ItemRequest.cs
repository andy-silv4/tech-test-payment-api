namespace Pottencial.Payment.Application.V1.DTOs;

public class ItemRequest
{
    public string CodigoProduto { get; private set; } = null!;
    public string NomeProduto { get; private set; } = null!;
    public int Quantidade { get; private set; }
    public decimal ValorUnidade { get; private set; }

    public ItemRequest(string codigoProduto, string nomeProduto,
                       int quantidade, decimal valorUnidade)
    {
        CodigoProduto = codigoProduto;
        NomeProduto = nomeProduto;
        Quantidade = quantidade;
        ValorUnidade = valorUnidade;
    }
}
