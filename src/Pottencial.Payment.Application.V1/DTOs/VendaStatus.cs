namespace Pottencial.Payment.Application.V1.DTOs;

public enum VendaStatus
{
    AguardandoPagamento,
    PagamentoAprovado,
    EnviadoParaTransportadora,
    Entregue,
    Cancelado
}
