namespace Pottencial.Payment.Application.V1.DTOs;

public class VendedorRequest
{
    public string Cpf { get; set; }
    public string? Nome { get; set; }
    public string? Sobrenome { get; set; }
    public string? Email { get; set; }
    public string? Ddd { get; set; }
    public string? Telefone { get; set; }

    public VendedorRequest(string cpf, string? nome,
                           string? sobrenome, string? email,
                           string? ddd, string? telefone)
    {
        Cpf = cpf;
        Nome = nome;
        Sobrenome = sobrenome;
        Email = email;
        Ddd = ddd;
        Telefone = telefone;
    }
}
