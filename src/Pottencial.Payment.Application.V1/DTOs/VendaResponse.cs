namespace Pottencial.Payment.Application.V1.DTOs;

public class VendaResponse
{
    public Guid Id { get; set; }
    public DateTime DataVenda { get; set; }
    public string VendaStatus { get; set; }
    public VendedorResponse Vendedor { get; set; }
    public IEnumerable<ItemResponse> Items { get; set; }
    public decimal ValorTotal { get; set; }

    public VendaResponse(Guid id, DateTime dataVenda,
                         string vendaStatus, VendedorResponse vendedor,
                         IEnumerable<ItemResponse> items, decimal valorTotal)
    {
        Id = id;
        DataVenda = dataVenda;
        VendaStatus = vendaStatus;
        Vendedor = vendedor;
        Items = items;
        ValorTotal = valorTotal;
    }

    public override bool Equals(object? obj)
    {
        return obj is VendaResponse response &&
               Id.Equals(response.Id) &&
               DataVenda == response.DataVenda &&
               VendaStatus == response.VendaStatus &&
               Vendedor.Equals(response.Vendedor) &&
               Items.SequenceEqual(response.Items) &&
               ValorTotal == response.ValorTotal;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Id, DataVenda, VendaStatus, Vendedor, Items, ValorTotal);
    }
}
