namespace Pottencial.Payment.Application.V1.DTOs;

public class ErroResponse
{
    public IList<string> Erros { get; set; }

    public ErroResponse(string erro)
    {
        Erros = new List<string>() { erro };
    }

    public ErroResponse(IList<string> erros)
    {
        Erros = erros;
    }
}
