namespace Pottencial.Payment.Application.V1.DTOs;

public class VendedorResponse
{
    public Guid Id { get; set; }
    public string Cpf { get; set; }
    public string NomeCompleto { get; set; }
    public string Email { get; set; }
    public string TelefoneComDdd { get; set; }

    public VendedorResponse(Guid id, string cpf, string nomeCompleto,
                            string email, string telefonComDdd)
    {
        Id = id;
        Cpf = cpf;
        NomeCompleto = nomeCompleto;
        Email = email;
        TelefoneComDdd = telefonComDdd;
    }

    public override bool Equals(object? obj)
    {
        return obj is VendedorResponse response &&
               Id.Equals(response.Id) &&
               Cpf == response.Cpf &&
               NomeCompleto == response.NomeCompleto &&
               Email == response.Email &&
               TelefoneComDdd == response.TelefoneComDdd;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Cpf, NomeCompleto, Email, TelefoneComDdd);
    }
}
