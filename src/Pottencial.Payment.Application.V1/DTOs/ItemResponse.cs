namespace Pottencial.Payment.Application.V1.DTOs;

public class ItemResponse : ItemRequest
{
    public decimal ValorTotal { get; private set; }

    public ItemResponse(string codigoProduto, string nomeProduto,
                        int quantidade, decimal valorUnidade,
                        decimal valorTotal)
        : base(codigoProduto, nomeProduto,
               quantidade, valorUnidade)
    {
        ValorTotal = valorTotal;
    }

    public override bool Equals(object? obj)
    {
        return obj is ItemResponse response &&
               CodigoProduto == response.CodigoProduto &&
               NomeProduto == response.NomeProduto &&
               Quantidade == response.Quantidade &&
               ValorUnidade == response.ValorUnidade &&
               ValorTotal == response.ValorTotal;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(CodigoProduto, NomeProduto, Quantidade, ValorUnidade, ValorTotal);
    }
}
