using System.Text.Json.Serialization;

namespace Pottencial.Payment.Application.V1.DTOs;

public class VendaRequest
{
    public VendedorRequest Vendedor { get; set; }
    public IEnumerable<ItemRequest> Items { get; set; }

    public VendaRequest(VendedorRequest vendedor, IEnumerable<ItemRequest> items)
    {
        Vendedor = vendedor;
        Items = items;
    }
}
