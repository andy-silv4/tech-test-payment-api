using Microsoft.Extensions.Logging;
using Pottencial.Payment.API.Application.Services;
using Pottencial.Payment.API.Application.V1.Mappers;
using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V1.Services;

public class VendedorService : IVendedorService
{
    private readonly ILogger<VendedorService> _logger;
    private readonly INotificador _notificador;
    private readonly IVendedorRepository _vendedorRepository;

    public VendedorService(ILogger<VendedorService> logger,
                           INotificador notificador,
                           IVendedorRepository vendedorRepository)
    {
        _logger = logger;
        _notificador = notificador;
        _vendedorRepository = vendedorRepository;
    }

    public async Task<VendedorResponse> AdicionarVendedor(VendedorRequest request)
    {
        VendedorResponse response = null!;
        try
        {
            var vendedor = request.ParaEntity();

            response = await VerificarVendedor(vendedor);

            _logger.LogInformation($"Id {vendedor.Id} - Vendedor cadastrado - E-mail: {vendedor.Email}");
        }
        catch (DomainException exception)
        {
            _notificador.AdicionarNotificacao(exception.Message);
            _logger.LogWarning($"[{nameof(DomainException)}] Erro ao adicionar vendedor");
        }

        return response;
    }

    private async Task<VendedorResponse> VerificarVendedor(Vendedor novoVendedor)
    {
        var vendedor = await _vendedorRepository.BuscarVendedor(novoVendedor.Cpf);

        if (vendedor == null)
        {
            vendedor = novoVendedor;

            await _vendedorRepository.AdicionarVendedor(vendedor);
        }

        return vendedor.ParaOutput();
    }
}
