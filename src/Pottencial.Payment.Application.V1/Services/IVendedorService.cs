using Pottencial.Payment.Application.V1.DTOs;

namespace Pottencial.Payment.API.Application.Services;

public interface IVendedorService
{
    Task<VendedorResponse> AdicionarVendedor(VendedorRequest request);
}
