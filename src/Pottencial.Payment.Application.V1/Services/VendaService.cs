using Microsoft.Extensions.Logging;
using Pottencial.Payment.API.Application.Services;
using Pottencial.Payment.API.Application.V1.Mappers;
using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V1.Services;

public class VendaService : IVendaService
{
    public readonly ILogger<VendaService> _logger;
    public readonly INotificador _notificador;
    public readonly IVendaRepository _vendaRepository;
    public readonly IVendedorService _vendedorService;
    public readonly IStatusVendaService _statusVendaService;

    public VendaService(ILogger<VendaService> logger,
                        INotificador notificador,
                        IVendaRepository repository,
                        IVendedorService vendedorService,
                        IStatusVendaService statusVendaService)
    {
        _logger = logger;
        _notificador = notificador;
        _vendaRepository = repository;
        _vendedorService = vendedorService;
        _statusVendaService = statusVendaService;
    }

    public async Task<VendaResponse> BuscarVenda(Guid id)
    {
        _logger.LogInformation($"Venda - Request Id {id}");

        var venda = await _vendaRepository.BuscarVendaComInformacoes(id);

        return venda.ParaOutput();
    }

    public async Task<VendaResponse> AdicionarVenda(VendaRequest request)
    {
        VendaResponse response = null!;
        try
        {
            var venda = await GerarVenda(request);

            if (_notificador.ExistemNotificacoes) return null!;

            await _vendaRepository.AdicionarVenda(venda);

            response = venda.ParaOutput();

            await _vendaRepository.Commit();

            _logger.LogInformation($"Id {venda.Id} - Venda realizada - Valor: R$ {venda.ValorTotal}");
        }
        catch (DomainException exception)
        {
            _notificador.AdicionarNotificacao(exception.Message);
            _logger.LogWarning($"[{nameof(DomainException)}] Erro ao adicionar venda");
        }

        return response;
    }

    public async Task<bool> AtualizarStatusVenda(Guid id, VendaStatus vendaStatus)
    {
        bool atualizado = false;
        try
        {
            atualizado = await _statusVendaService.AtualizarStatus(id, (StatusVenda)vendaStatus);

            if (atualizado) await _vendaRepository.Commit();

            _logger.LogInformation($"Id {id} - Status Atualizado: {vendaStatus}");
        }
        catch (DomainException exception)
        {
            _notificador.AdicionarNotificacao(exception.Message);
            _logger.LogWarning($"[{nameof(DomainException)}] Erro ao atualizar status");
        }

        return atualizado;
    }

    private async Task<Venda> GerarVenda(VendaRequest request)
    {
        var vendedor = await _vendedorService.AdicionarVendedor(request.Vendedor);

        if (_notificador.ExistemNotificacoes) return null!;

        var venda = new Venda(vendedor.Id);

        var itens = request.Items.ParaEntity(venda.Id);

        venda.AdicionarItens(itens).CalcularValorTotal();

        return venda;
    }
}
