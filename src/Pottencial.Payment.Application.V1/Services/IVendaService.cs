using Pottencial.Payment.Application.V1.DTOs;

namespace Pottencial.Payment.Application.V1.Services;

public interface IVendaService
{
    Task<VendaResponse> BuscarVenda(Guid id);
    Task<VendaResponse> AdicionarVenda(VendaRequest request);
    Task<bool> AtualizarStatusVenda(Guid id, VendaStatus vendaStatus);
}
