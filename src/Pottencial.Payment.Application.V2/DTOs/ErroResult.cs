namespace Pottencial.Payment.Application.V2.DTOs;

public class ErroResult
{
    public IReadOnlyCollection<string> Erros { get; set; }

    public ErroResult(string erro)
    {
        Erros = new List<string>() { erro };
    }

    public ErroResult(IReadOnlyCollection<string> erros)
    {
        Erros = erros;
    }
}
