namespace Pottencial.Payment.Application.V2.DTOs;

public class ItemResponse
{
    public string CodigoProduto { get; set; }
    public string NomeProduto { get; set; }
    public int Quantidade { get; set; }
    public decimal ValorUnidade { get; set; }
    public decimal ValorTotal { get; private set; }

    public ItemResponse(string codigoProduto, string nomeProduto,
                       int quantidade, decimal valorUnidade,
                       decimal valorTotal)
    {
        CodigoProduto = codigoProduto;
        NomeProduto = nomeProduto;
        Quantidade = quantidade;
        ValorUnidade = valorUnidade;
        ValorTotal = valorTotal;
    }
}
