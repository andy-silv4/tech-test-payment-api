namespace Pottencial.Payment.Application.V2.DTOs;

public class ItemRequest
{
    public string CodigoProduto { get; set; }
    public string NomeProduto { get; set; }
    public int Quantidade { get; set; }
    public decimal ValorUnidade { get; set; }

    public ItemRequest(string codigoProduto, string nomeProduto,
                       int quantidade, decimal valorUnidade)
    {
        CodigoProduto = codigoProduto;
        NomeProduto = nomeProduto;
        Quantidade = quantidade;
        ValorUnidade = valorUnidade;
    }
}
