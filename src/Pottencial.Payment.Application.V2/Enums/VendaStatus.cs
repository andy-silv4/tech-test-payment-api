namespace Pottencial.Payment.Application.V2.Enums;

public enum VendaStatus
{
    AguardandoPagamento,
    PagamentoAprovado,
    EnviadoParaTransportadora,
    Entregue,
    Cancelado
}
