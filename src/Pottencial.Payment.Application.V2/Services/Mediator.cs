using Microsoft.Extensions.DependencyInjection;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V2.Services;

public class Mediator : IMediator
{
    private readonly IServiceProvider _serviceProvider;

    public Mediator(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task<TCommandResult> Send<TCommand, TCommandResult>(TCommand request)
        where TCommand : IRequest
        where TCommandResult : IResponse
    {
        var handler = _serviceProvider.GetRequiredService<IRequestHandler<TCommand, TCommandResult>>();

        return await handler.Handle(request);
    }

    public async Task Publish<TEvent>(TEvent message)
        where TEvent : IEvent
    {
        var handler = _serviceProvider.GetRequiredService<IEventHandler<TEvent>>();

        await handler.Handle(message);
    }
}
