using Pottencial.Payment.Application.V2.DTOs;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Application.V2.Mappers;

public static class ItemVendaMapper
{
    public static IEnumerable<VendaItem> ParaEntity(this IEnumerable<ItemRequest> input, Guid vendaId)
    {
        var itens = new List<VendaItem>();

        if (input != null)
        {
            int ordem = 1;
            itens = input.Select(x =>
                new VendaItem(vendaId, x.CodigoProduto, ordem++,
                              x.NomeProduto, x.Quantidade, x.ValorUnidade)
            ).ToList();
        }

        return itens;
    }

    public static IEnumerable<ItemResponse> ParaItemResponse(this IEnumerable<VendaItem> itens)
    {
        var response = new List<ItemResponse>();

        if (itens != null)
        {
            response = itens.Select(x =>
                new ItemResponse(x.ProdutoId, x.NomeProduto, x.Quantidade, x.ValorUnidade, x.ValorTotal())
            ).ToList();
        }

        return response;
    }
}
