using Pottencial.Payment.Application.V2.UseCases.Vendas.BuscarVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Application.V2.Mappers;

public static class VendaMapper
{
    public static VendaCommandResult ParaCommandResult(this Venda venda)
    {
        var result = new VendaCommandResult();

        if (venda != null)
        {
            var itensResponse = venda.Itens.ParaItemResponse();

            result.Id = venda.Id;
            result.DataVenda = venda.DataVenda;
            result.VendaStatus = venda.Status.ToString();
            result.Items = itensResponse;
            result.ValorTotal = venda.ValorTotal;
        }

        return result;
    }

    public static VendaQueryResult ParaQueryResult(this Venda venda)
    {
        var result = new VendaQueryResult();

        if (venda != null)
        {
            var itensResponse = venda.Itens.ParaItemResponse();

            result.Id = venda.Id;
            result.DataVenda = venda.DataVenda;
            result.Vendedor = venda.Vendedor!.Nome.NomeCompleto();
            result.VendaStatus = venda.Status.ToString();
            result.Items = itensResponse;
            result.ValorTotal = venda.ValorTotal;
        }

        return result;
    }
}
