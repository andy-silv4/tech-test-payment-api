using Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;
using Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.ValueObjects;

namespace Pottencial.Payment.Application.V2.Mappers;

public static class VendedorMapper
{
    public static Vendedor ParaEntity(this CadastrarVendedorCommand command)
    {
        Vendedor vendedor = default!;

        if (command != null)
        {
            vendedor = new Vendedor(new Nome(command.Nome!, command.Sobrenome!),
                                    new Cpf(command.Cpf),
                                    new Email(command.Email!),
                                    new Telefone(command.Ddd!, command.Telefone!));
        }

        return vendedor;
    }

    public static VendedorCommandResult ParaCommandResult(this Vendedor vendedor)
    {
        var result = new VendedorCommandResult();

        if (vendedor != null)
        {
            result.Id = vendedor!.Id;
            result.Cpf = vendedor.Cpf.CpfMascarado();
            result.NomeCompleto = vendedor.Nome.NomeCompleto();
            result.Email = vendedor.Email.Endereco;
            result.TelefoneComDdd = vendedor.Telefone.TelefoneCompleto();
        }

        return result;
    }

    public static VendedorQueryResult ParaQueryResult(this Vendedor vendedor)
    {
        var result = new VendedorQueryResult();

        if (vendedor != null)
        {
            result.Id = vendedor!.Id;
            result.Cpf = vendedor.Cpf.CpfMascarado();
            result.NomeCompleto = vendedor.Nome.NomeCompleto();
            result.Email = vendedor.Email.Endereco;
            result.TelefoneComDdd = vendedor.Telefone.TelefoneCompleto();
        }

        return result;
    }
}
