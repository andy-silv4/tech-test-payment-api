using Microsoft.Extensions.Logging;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Application.V2.Mappers;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.BuscarVenda;

public class BuscarVendaHandler : IQueryHandler<BuscarVendaPorIdQuery, VendaQueryResult>
{
    private readonly ILogger<BuscarVendaHandler> _logger;
    private readonly IVendaRepository _repository;

    public BuscarVendaHandler(ILogger<BuscarVendaHandler> logger,
                              IVendaRepository repository)
    {
        _logger = logger;
        _repository = repository;
    }

    public async Task<VendaQueryResult> Handle(BuscarVendaPorIdQuery query)
    {
        _logger.LogInformation($"Venda - Request Id {query.Id}");

        var result = new VendaQueryResult();
        var venda = await _repository.BuscarVendaComInformacoes(query.Id);

        return venda.ParaQueryResult();
    }
}
