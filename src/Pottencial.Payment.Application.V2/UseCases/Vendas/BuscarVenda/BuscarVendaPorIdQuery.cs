using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.BuscarVenda;

public class BuscarVendaPorIdQuery : IQuery
{
    public Guid Id { get; set; }

    public BuscarVendaPorIdQuery(Guid id)
    {
        Id = id;
    }
}
