using Pottencial.Payment.Application.V2.DTOs;
using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.BuscarVenda;

public class VendaQueryResult : IQueryResult
{
    public Guid Id { get; set; }
    public DateTime DataVenda { get; set; }
    public string Vendedor { get; set; }
    public string VendaStatus { get; set; }
    public IEnumerable<ItemResponse> Items { get; set; }
    public decimal ValorTotal { get; set; }

    public VendaQueryResult()
    {
        Id = Guid.Empty;
        DataVenda = DateTime.Now;
        Vendedor = string.Empty;
        VendaStatus = string.Empty;
        Items = new List<ItemResponse>();
        ValorTotal = decimal.MinValue;
    }
}
