using Microsoft.Extensions.Logging;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Events;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda.Events;

public class VendaRegistradaHandler : IEventHandler<VendaRegistradaEvent>
{
    private readonly ILogger<VendaRegistradaHandler> _logger;
    private readonly ApplicationDbContext _context;

    public VendaRegistradaHandler(ILogger<VendaRegistradaHandler> logger,
                                  ApplicationDbContext context)
    {
        _logger = logger;
        _context = context;
    }

    public async Task Handle(VendaRegistradaEvent evento)
    {
        var mensagem = $"Evento 'VendaRegistrada', Id {evento.Venda.Id} com valor total de {evento.Venda.ValorTotal} lançado em {evento.CriadoEm}";
        var entity = new Evento(evento.Venda.Id, mensagem);

        await _context.Eventos.AddAsync(entity);
        await _context.SaveChangesAsync();

        _logger.LogInformation(mensagem);
    }
}
