using Microsoft.Extensions.Logging;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Application.V2.Mappers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Events;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda;

public class RegistrarVendaHandler : ICommandHandler<RegistrarVendaCommand, VendaCommandResult>
{
    public readonly ILogger<RegistrarVendaHandler> _logger;
    public readonly INotificador _notificador;
    public readonly IUnitOfWork _unitOfWork;
    public readonly IEventDispatcher _dispatcher;

    public RegistrarVendaHandler(ILogger<RegistrarVendaHandler> logger,
                                 INotificador notificador,
                                 IUnitOfWork unitOfWork,
                                 IEventDispatcher dispatcher)
    {
        _logger = logger;
        _notificador = notificador;
        _unitOfWork = unitOfWork;
        _dispatcher = dispatcher;
    }

    public async Task<VendaCommandResult> Handle(RegistrarVendaCommand command)
    {
        var result = new VendaCommandResult();
        try
        {
            var existe = await VerificaVendedor(command.VendedorId);

            if (existe)
            {
                var venda = GeraVenda(command);

                await _unitOfWork.VendaRepository.AdicionarVenda(venda);
                var sucesso = await _unitOfWork.Commit();

                if (sucesso)
                    await _dispatcher.Publish(new StatusAtualizadoEvent(venda));

                result = venda.ParaCommandResult();

                _logger.LogInformation($"Id {venda.Id} - Venda realizada - Valor: R$ {venda.ValorTotal}");
            }
        }
        catch (DomainException exception)
        {
            _notificador.AdicionarNotificacao(exception.Message);
            _logger.LogWarning($"[{nameof(DomainException)}] Erro ao registrar venda");
        }

        return result;
    }

    private async Task<bool> VerificaVendedor(Guid id)
    {
        var existe = await _unitOfWork.VendedorRepository.ExisteVendedor(id);

        if (!existe)
            throw new DomainException("O id do vendedor informado nao existe");

        return existe;
    }

    private Venda GeraVenda(RegistrarVendaCommand command)
    {
        var venda = new Venda(command.VendedorId);

        var itens = command.Items.ParaEntity(venda.Id);

        venda.AdicionarItens(itens)
            .CalcularValorTotal();

        venda.AdicionaEvento(new VendaRegistradaEvent(venda));

        return venda;
    }
}
