using Pottencial.Payment.Application.V2.DTOs;
using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda;

public class RegistrarVendaCommand : ICommand
{
    public Guid VendedorId { get; set; }
    public IEnumerable<ItemRequest> Items { get; set; }

    public RegistrarVendaCommand(Guid vendedorId, IEnumerable<ItemRequest> items)
    {
        VendedorId = vendedorId;
        Items = items;
    }
}
