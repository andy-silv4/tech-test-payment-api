using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda;

public class StatusVendaCommandResult : ICommandResult
{
    public bool Atualizado { get; set; }

    public StatusVendaCommandResult(bool atualizado)
    {
        Atualizado = atualizado;
    }
}
