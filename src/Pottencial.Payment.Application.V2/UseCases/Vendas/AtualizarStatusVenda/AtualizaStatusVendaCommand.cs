using System.Text.Json.Serialization;
using Pottencial.Payment.Application.V2.Enums;
using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda;

public class AtualizarStatusVendaCommand : ICommand
{
    [JsonIgnore]
    public Guid Id { get; set; }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public VendaStatus VendaStatus { get; set; }

    public AtualizarStatusVendaCommand(VendaStatus vendaStatus)
    {
        VendaStatus = vendaStatus;
    }
}
