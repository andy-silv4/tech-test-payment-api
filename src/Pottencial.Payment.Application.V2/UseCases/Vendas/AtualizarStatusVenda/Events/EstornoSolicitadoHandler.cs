using Microsoft.Extensions.Logging;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Events;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda.Events;

public class EstornoSolicitadoHandler : IEventHandler<EstornoSolicitadoEvent>
{
    private readonly ILogger<EstornoSolicitadoHandler> _logger;
    private readonly ApplicationDbContext _context;

    public EstornoSolicitadoHandler(ILogger<EstornoSolicitadoHandler> logger,
                                    ApplicationDbContext context)
    {
        _logger = logger;
        _context = context;
    }

    public async Task Handle(EstornoSolicitadoEvent evento)
    {
        var mensagem = $"Evento 'EstornoSolicitado', Id {evento.Id} com valor total de {evento.ValorTotal} lançado em {evento.CriadoEm}";
        var entity = new Evento(evento.Id, mensagem);

        await _context.Eventos.AddAsync(entity);
        await _context.SaveChangesAsync();

        _logger.LogInformation(mensagem);
    }
}
