using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Events;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda.Events;

public class StatusAtualizadoHandler : IEventHandler<StatusAtualizadoEvent>
{
    private readonly ILogger<StatusAtualizadoHandler> _logger;
    private readonly ApplicationDbContext _context;

    public StatusAtualizadoHandler(IServiceProvider serviceProvider)
    {
        _logger = serviceProvider.GetRequiredService<ILogger<StatusAtualizadoHandler>>();
        _context = serviceProvider.GetRequiredService<ApplicationDbContext>();
    }

    public async Task Handle(StatusAtualizadoEvent evento)
    {
        var mensagem = $"Evento '{evento.StatusVenda}' de Id {evento.Id} lançado em {evento.CriadoEm}";
        var entity = new Evento(evento.Id, mensagem);

        await _context.Eventos.AddAsync(entity);
        await _context.SaveChangesAsync();

        _logger.LogInformation(mensagem);
    }
}
