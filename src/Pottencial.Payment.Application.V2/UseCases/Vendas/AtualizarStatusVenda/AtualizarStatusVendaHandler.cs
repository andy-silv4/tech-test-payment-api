using Microsoft.Extensions.Logging;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda;

public class AtualizarStatusVendaHandler : ICommandHandler<AtualizarStatusVendaCommand, StatusVendaCommandResult>
{
    private readonly ILogger<AtualizarStatusVendaHandler> _logger;
    private readonly INotificador _notificador;
    private readonly IStatusVendaService _statusVendaService;

    public AtualizarStatusVendaHandler(ILogger<AtualizarStatusVendaHandler> logger,
                                       INotificador notificador,
                                       IStatusVendaService statusVendaService)
    {
        _logger = logger;
        _notificador = notificador;
        _statusVendaService = statusVendaService;
    }

    public async Task<StatusVendaCommandResult> Handle(AtualizarStatusVendaCommand command)
    {
        var result = new StatusVendaCommandResult(false);
        try
        {
            result.Atualizado = await _statusVendaService
                .AtualizarStatus(command.Id, (StatusVenda)command.VendaStatus);

            _logger.LogInformation($"Id {command.Id} - Status Atualizado: {command.VendaStatus}");
        }
        catch (DomainException exception)
        {
            _notificador.AdicionarNotificacao(exception.Message);
            _logger.LogWarning($"[{nameof(DomainException)}] Erro ao atualizar status");
        }

        return result;
    }
}
