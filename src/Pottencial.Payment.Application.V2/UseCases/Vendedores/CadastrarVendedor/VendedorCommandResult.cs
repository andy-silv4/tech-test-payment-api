using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;

public class VendedorCommandResult : ICommandResult
{
    public Guid Id { get; set; }
    public string Cpf { get; set; }
    public string NomeCompleto { get; set; }
    public string Email { get; set; }
    public string TelefoneComDdd { get; set; }

    public VendedorCommandResult()
    {
        Id = Guid.Empty;
        Cpf = string.Empty;
        NomeCompleto = string.Empty;
        Email = string.Empty;
        TelefoneComDdd = string.Empty;
    }
}
