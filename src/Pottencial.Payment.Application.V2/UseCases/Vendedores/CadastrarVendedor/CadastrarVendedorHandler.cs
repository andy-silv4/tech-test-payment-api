using Microsoft.Extensions.Logging;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Application.V2.Mappers;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;

public class CadastrarVendedorHandler : ICommandHandler<CadastrarVendedorCommand, VendedorCommandResult>
{
    private readonly ILogger<CadastrarVendedorHandler> _logger;
    private readonly INotificador _notificador;
    private readonly IUnitOfWork _unitOfWork;

    public CadastrarVendedorHandler(ILogger<CadastrarVendedorHandler> logger,
                                    INotificador notificador,
                                    IUnitOfWork unitOfWork)
    {
        _logger = logger;
        _notificador = notificador;
        _unitOfWork = unitOfWork;
    }

    public async Task<VendedorCommandResult> Handle(CadastrarVendedorCommand command)
    {
        var result = new VendedorCommandResult();
        try
        {
            var vendedor = command.ParaEntity();
            var existe = await _unitOfWork.VendedorRepository.ExisteVendedor(vendedor);

            if (existe)
                throw new DomainException("O vendedor com o email ou cpf informado já está cadastrado");

            await _unitOfWork.VendedorRepository.AdicionarVendedor(vendedor);
            await _unitOfWork.Commit();

            result = vendedor.ParaCommandResult();

            _logger.LogInformation($"Id {vendedor.Id} - Vendedor cadastrado - E-mail: {command.Email}");
        }
        catch (DomainException exception)
        {
            _notificador.AdicionarNotificacao(exception.Message);
            _logger.LogWarning($"[{nameof(DomainException)}] Erro ao cadastrar vendedor");
        }

        return result;
    }
}
