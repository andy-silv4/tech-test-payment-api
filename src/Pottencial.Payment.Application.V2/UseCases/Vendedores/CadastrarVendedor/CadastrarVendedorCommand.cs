using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;

public class CadastrarVendedorCommand : ICommand
{
    public string Cpf { get; set; }
    public string Nome { get; set; }
    public string Sobrenome { get; set; }
    public string Email { get; set; }
    public string Ddd { get; set; }
    public string Telefone { get; set; }

    public CadastrarVendedorCommand(string cpf, string nome,
                                string sobrenome, string email,
                                string ddd, string telefone)
    {
        Cpf = cpf;
        Nome = nome;
        Sobrenome = sobrenome;
        Email = email;
        Ddd = ddd;
        Telefone = telefone;
    }
}
