using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;

public class BuscarVendedorPorEmailQuery : IQuery
{
    public string Email { get; set; }

    public BuscarVendedorPorEmailQuery(string email)
    {
        Email = email;
    }
}
