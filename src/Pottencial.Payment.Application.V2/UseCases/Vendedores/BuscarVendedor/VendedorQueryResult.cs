using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;

public class VendedorQueryResult : IQueryResult
{
    public Guid Id { get; set; }
    public string Cpf { get; set; }
    public string NomeCompleto { get; set; }
    public string Email { get; set; }
    public string TelefoneComDdd { get; set; }

    public VendedorQueryResult()
    {
        Id = Guid.Empty;
        Cpf = string.Empty;
        NomeCompleto = string.Empty;
        Email = string.Empty;
        TelefoneComDdd = string.Empty;
    }
}
