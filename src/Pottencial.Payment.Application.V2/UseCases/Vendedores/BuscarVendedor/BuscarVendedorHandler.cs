using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using Pottencial.Payment.Application.V2.Interfaces;
using Pottencial.Payment.Application.V2.Mappers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;

public class BuscarVendedorHandler : IQueryHandler<BuscarVendedorPorIdQuery, VendedorQueryResult>,
                                     IQueryHandler<BuscarVendedorPorEmailQuery, VendedorQueryResult>
{
    private readonly ILogger<BuscarVendedorHandler> _logger;
    private readonly IVendedorRepository _repository;

    public BuscarVendedorHandler(ILogger<BuscarVendedorHandler> logger,
                                 IVendedorRepository repository)
    {
        _logger = logger;
        _repository = repository;
    }

    public async Task<VendedorQueryResult> Handle(BuscarVendedorPorIdQuery query)
    {
        _logger.LogInformation($"Vendedor - Request Id {query.Id}");

        return await BuscaVendedor(x => x.Id == query.Id);
    }

    public async Task<VendedorQueryResult> Handle(BuscarVendedorPorEmailQuery query)
    {
        _logger.LogInformation($"Vendedor - Request por e-mail");

        return await BuscaVendedor(x => x.Email.Endereco == query.Email);
    }

    private async Task<VendedorQueryResult> BuscaVendedor(Expression<Func<Vendedor, bool>> expressao)
    {
        var result = new VendedorQueryResult();
        var vendedor = await _repository.BuscarVendedor(expressao);

        return vendedor.ParaQueryResult();
    }
}
