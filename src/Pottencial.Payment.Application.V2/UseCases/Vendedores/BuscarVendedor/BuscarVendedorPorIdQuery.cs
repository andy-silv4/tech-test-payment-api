using Pottencial.Payment.Application.V2.Interfaces;

namespace Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;

public class BuscarVendedorPorIdQuery : IQuery
{
    public Guid Id { get; set; }

    public BuscarVendedorPorIdQuery(Guid id)
    {
        Id = id;
    }
}
