using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Application.V2.Interfaces;

public interface IMediator
{
    Task<TResponse> Send<TRequest, TResponse>(TRequest request)
        where TRequest : IRequest
        where TResponse : IResponse;
    Task Publish<TEvent>(TEvent request)
        where TEvent : IEvent;
}
