namespace Pottencial.Payment.Application.V2.Interfaces;

public interface IRequestHandler { }

public interface IRequestHandler<TRequest, TResponse> : IRequestHandler
    where TRequest : IRequest
    where TResponse : IResponse
{
    Task<TResponse> Handle(TRequest command);
}
