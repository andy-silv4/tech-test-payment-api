namespace Pottencial.Payment.Application.V2.Interfaces;

public interface ICommandHandler<TCommand, TCommandResult> : IRequestHandler<TCommand, TCommandResult>
                                                             where TCommand : IRequest
                                                             where TCommandResult : IResponse
{ }
