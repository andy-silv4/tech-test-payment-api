namespace Pottencial.Payment.Application.V2.Interfaces;

public interface IQueryHandler<TQuery, TQueryResult> : IRequestHandler<TQuery, TQueryResult>
                                                       where TQuery : IRequest
                                                       where TQueryResult : IResponse
{ }
