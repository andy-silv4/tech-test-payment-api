using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Domain.ValueObjects;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.Infra.Repositories;

public class VendedorRepository : IVendedorRepository
{
    private readonly ApplicationDbContext _context;

    public VendedorRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Vendedor> BuscarVendedor(Expression<Func<Vendedor, bool>> query)
    {
        return (await _context.Vendedores.Where(query).FirstOrDefaultAsync())!;
    }

    public async Task<Vendedor> BuscarVendedor(Cpf cpf)
    {
        return (await _context.Vendedores.Where(x => x.Cpf.Numero == cpf.Numero).FirstOrDefaultAsync())!;
    }

    public async Task AdicionarVendedor(Vendedor vendedor)
    {
        _context.Vendedores.Add(vendedor);

        await Task.CompletedTask;
    }

    public async Task<bool> ExisteVendedor(Vendedor vendedor)
    {
        return await _context.Vendedores.AnyAsync(x => x.Cpf.Numero == vendedor.Cpf.Numero ||
            x.Email.Endereco == vendedor.Email.Endereco);
    }

    public async Task<bool> ExisteVendedor(Guid id)
    {
        return await _context.Vendedores.AnyAsync(x => x.Id == id);
    }

    public void Dispose()
    {
        _context?.Dispose();
    }
}
