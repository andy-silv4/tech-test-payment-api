using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.Infra.Repositories;

public class VendaRepository : IVendaRepository
{
    private readonly ApplicationDbContext _context;

    public VendaRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Venda> BuscarVenda(Guid id)
    {
        return (await _context.Vendas.FindAsync(id))!;
    }

    public async Task<Venda> BuscarVendaComInformacoes(Guid id)
    {
        return (await _context.Vendas.AsNoTracking()
                        .Include(x => x.Vendedor)
                        .Include(y => y.Itens.OrderBy(x => x.OrdemItem))
                        .Where(x => x.Id == id)
                        .FirstOrDefaultAsync())!;
    }

    public async Task AdicionarVenda(Venda venda)
    {
        _context.VendaItens.AddRange(venda.Itens);
        _context.Vendas.Add(venda);

        await Task.CompletedTask;
    }

    public async Task AtualizarVenda(Venda venda)
    {
        _context.Vendas.Update(venda);

        await Task.CompletedTask;
    }

    public async Task Commit() => await _context.SaveChangesAsync();

    public void Dispose()
    {
        _context?.Dispose();
    }
}
