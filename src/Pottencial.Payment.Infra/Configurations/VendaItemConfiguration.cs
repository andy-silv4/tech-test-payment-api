using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Infra.Configurations;

public class VendaItemConfiguration : IEntityTypeConfiguration<VendaItem>
{
    public void Configure(EntityTypeBuilder<VendaItem> builder)
    {
        builder.HasKey(v => v.Id);

        builder.HasOne(v => v.Venda)
            .WithMany(x => x.Itens)
            .HasForeignKey(z => z.VendaId);

        builder.Property(x => x.OrdemItem)
            .HasColumnName("Ordem")
            .IsRequired();

        builder.Property(x => x.ProdutoId)
            .HasMaxLength(36)
            .IsRequired();

        builder.Property(x => x.NomeProduto)
            .HasColumnName("Produto")
            .HasMaxLength(36)
            .IsRequired();

        builder.Property(x => x.Quantidade)
            .IsRequired();

        builder.Property(x => x.ValorUnidade)
            .HasColumnName("Valor")
            .IsRequired();

        builder.Ignore(x => x.Erros);

        builder.Ignore(x => x.Eventos);

        builder.ToTable("VendaItens");
    }
}
