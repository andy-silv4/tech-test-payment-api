using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Infra.Configurations;
public class VendedorConfiguration : IEntityTypeConfiguration<Vendedor>
{
    public void Configure(EntityTypeBuilder<Vendedor> builder)
    {
        builder.HasKey(v => v.Id);

        builder.OwnsOne(x => x.Nome, y =>
        {
            y.Property(p => p.PrimeiroNome).HasColumnName("Nome")
                .HasMaxLength(50)
                .IsRequired();

            y.Property(p => p.Sobrenome).HasColumnName("Sobrenome")
                .HasMaxLength(50)
                .IsRequired();

            y.Ignore(z => z.Erros);
        });

        builder.OwnsOne(x => x.Cpf, y =>
        {
            y.Property(p => p.Numero).HasColumnName("CPF")
                .HasMaxLength(11)
                .IsRequired();

            y.HasIndex(i => i.Numero).IsUnique();

            y.Ignore(z => z.Erros);
        });

        builder.OwnsOne(x => x.Email, y =>
        {
            y.Property(p => p.Endereco).HasColumnName("Email")
                .HasMaxLength(30)
                .IsRequired();

            y.Ignore(z => z.Erros);
        });

        builder.OwnsOne(x => x.Telefone, y =>
        {
            y.Property(p => p.Ddd).HasColumnName("DDD")
                .HasMaxLength(2)
                .IsRequired();

            y.Property(p => p.Numero).HasColumnName("Telefone")
                .HasMaxLength(9)
                .IsRequired();

            y.Ignore(z => z.Erros);
        });

        builder.Ignore(x => x.Erros);

        builder.Ignore(x => x.Eventos);

        builder.ToTable("Vendedores");
    }
}
