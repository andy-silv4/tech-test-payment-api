using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Infra.Configurations;

public class VendaConfiguration : IEntityTypeConfiguration<Venda>
{
    public void Configure(EntityTypeBuilder<Venda> builder)
    {
        builder.HasKey(v => v.Id);

        builder.HasOne(v => v.Vendedor);

        builder.HasMany(v => v.Itens);

        builder.Property(x => x.DataVenda)
            .HasColumnName("VendidoEm")
            .IsRequired();

        builder.Property(x => x.Status)
            .IsRequired();

        builder.Property(x => x.ValorTotal)
            .IsRequired();

        builder.Ignore(x => x.Erros);

        builder.Ignore(x => x.Eventos);

        builder.ToTable("Vendas");
    }
}
