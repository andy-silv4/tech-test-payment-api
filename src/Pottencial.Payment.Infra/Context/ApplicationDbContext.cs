using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Infra.Context;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options) { }

    public DbSet<Venda> Vendas { get; set; } = null!;
    public DbSet<VendaItem> VendaItens { get; set; } = null!;
    public DbSet<Vendedor> Vendedores { get; set; } = null!;

    public DbSet<Evento> Eventos { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}
