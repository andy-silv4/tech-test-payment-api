using Microsoft.Extensions.Logging;
using Pottencial.Payment.Domain.Events.Handlers;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Infra.Context;

public class EventDispatcher : IEventDispatcher, IEventDispatcherRegister
{
    private readonly ILogger<EventDispatcher> _logger;
    private Dictionary<Type, IList<IEventHandler>> _eventHandlers;

    public EventDispatcher(ILogger<EventDispatcher> logger)
    {
        _logger = logger;
        _eventHandlers = new Dictionary<Type, IList<IEventHandler>>();
    }

    public void Register<TEvent>(IEventHandler<TEvent> handler)
        where TEvent : IEvent
    {
        if (!_eventHandlers.ContainsKey(typeof(TEvent)))
            _eventHandlers[typeof(TEvent)] = new List<IEventHandler>();

        _eventHandlers[typeof(TEvent)].Add(handler);
    }

    public void Unregister<TEvent>()
        where TEvent : IEvent
    {
        _eventHandlers.Remove(typeof(TEvent));
    }

    public async Task Publish<TEvent>(TEvent evento)
        where TEvent : IEvent
    {
        try
        {
            var eventType = evento.GetType();

            if (_eventHandlers.TryGetValue(eventType, out var handlers))
            {
                foreach (var handler in handlers)
                {
                    if (handler is IEventHandler<TEvent>)
                    {
                        var eventHandler = (IEventHandler<TEvent>)handler;

                        await eventHandler.Handle(evento);

                        continue;
                    }

                    var genericType = typeof(DecoratorEventHandler<>).MakeGenericType(eventType);

                    var baseHandler = (IDecoratorEventHandler)(Activator.CreateInstance(genericType, handler))!;

                    await baseHandler.Handle(evento);
                }

                _logger.LogInformation($"Evento '{evento}' publicado");

                return;
            }
        }
        catch (Exception)
        {
            _logger.LogError($"Erro ao publicar evento '{evento}'");

            throw;
        }
    }
}
