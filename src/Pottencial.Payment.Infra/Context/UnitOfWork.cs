using Microsoft.Extensions.Logging;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces;

namespace Pottencial.Payment.Infra.Context;

public class UnitOfWork : IUnitOfWork, IDisposable
{
    private readonly ILogger<UnitOfWork> _logger;
    private readonly ApplicationDbContext _context;
    private readonly IEventDispatcher _dispatcher;

    private readonly IVendaRepository _vendaRepository;
    private readonly IVendedorRepository _vendedorRepository;


    public UnitOfWork(ILogger<UnitOfWork> logger,
                      ApplicationDbContext context,
                      IEventDispatcher dispatcher,
                      IVendaRepository vendaRepository,
                      IVendedorRepository vendedorRepository)
    {
        _logger = logger;
        _context = context;
        _dispatcher = dispatcher;

        _vendaRepository = vendaRepository;
        _vendedorRepository = vendedorRepository;
    }

    public IVendaRepository VendaRepository => _vendaRepository;
    public IVendedorRepository VendedorRepository => _vendedorRepository;

    public async Task<bool> Commit()
    {
        int alteracoes = await _context.SaveChangesAsync();
        var sucesso = alteracoes > 0;

        if (sucesso)
        {
            var entities = _context.ChangeTracker
                        .Entries<Entity>()
                        .Where(x => x.Entity.Eventos.Any())
                        .Select(x => x.Entity);

            await EnviarEventos(entities);

            entities.ToList()
                .ForEach(x => x.RemoveEventos());
        }

        _logger.LogInformation($"Banco de dados - {alteracoes} alteração(ões) concluídas");

        return sucesso;
    }

    public void Dispose()
    {
        _context?.Dispose();
        GC.SuppressFinalize(this);
    }

    private async Task EnviarEventos(IEnumerable<Entity> entities)
    {
        var events = entities.SelectMany(x =>
            x.Eventos).ToList();

        var tasks = events.Select(async message =>
            await _dispatcher.Publish(message));

        await Task.WhenAll(tasks);
    }
}
