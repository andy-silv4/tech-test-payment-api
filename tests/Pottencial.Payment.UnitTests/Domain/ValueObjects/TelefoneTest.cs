using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.ValueObjects;
using Xunit;

namespace Pottencial.Payment.UnitTests.Domain.ValueObjects;

public class TelefoneTest
{

    [Theory]
    [InlineData("31", "987654321")]
    [InlineData("31", "36819999")]
    public void NovoTelefone_ComDddENumeroValido_DeveSerValido(string ddd, string numero)
    {
        // Arrange
        var telefone = new Telefone(ddd, numero);

        // Act
        var valido = telefone.EhValido();

        // Assert
        Assert.True(valido);
        Assert.Equal(ddd, telefone.Ddd);
        Assert.Equal(numero, telefone.Numero);
    }

    [Theory]
    [InlineData("31", "987654321", "(31) 98765-4321")]
    [InlineData("31", "36819999", "(31) 3681-9999")]
    public void NovoTelefone_ComDddENumeroValido_DeveRetornarTelefoneCompleto(string ddd, string numero, string expected)
    {
        // Arrange
        var telefone = new Telefone(ddd, numero);

        // Act
        var telefoneCompleto = telefone.TelefoneCompleto();

        // Assert
        Assert.Equal(expected, telefoneCompleto);
    }

    [Theory]
    [InlineData("0")]
    [InlineData("031")]
    public void NovoTelefone_ComTamanhoDddInvalido_DeveTerErros(string ddd)
    {
        // Arrange
        var expected = $"O ddd deve ter {Telefone.TamanhoDdd} digitos";

        // Act
        var telefone = new Telefone(ddd, "987654321");
        var erros = telefone.Erros;

        // Assert
        Assert.Equal(expected, erros.FirstOrDefault());
    }

    [Theory]
    [InlineData("7654321")]
    [InlineData("0987654321")]
    public void NovoTelefone_ComTamanhoNumeroInvalido_DeveTerErros(string numero)
    {
        // Arrange
        var expected = $"O telefone deve ter entre {Telefone.TamanhoMinimo} " +
                       $"e {Telefone.TamanhoMaximo} digitos";

        // Act
        var telefone = new Telefone("31", numero);
        var erros = telefone.Erros;

        // Assert
        Assert.Equal(expected, erros.FirstOrDefault());
    }

    [Theory]
    [InlineData("**", "987654321")]
    [InlineData("31", "*********")]
    public void NovoTelefone_ComTelefoneInvalido_DeveTerErros(string ddd, string numero)
    {
        // Arrange
        var expected = $"O telefone deve possuir apenas numeros";

        // Act
        var telefone = new Telefone(ddd, numero);
        var erros = telefone.Erros;

        // Assert
        Assert.Equal(expected, erros.FirstOrDefault());
    }
}
