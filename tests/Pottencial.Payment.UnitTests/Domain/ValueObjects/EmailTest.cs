using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.ValueObjects;
using Xunit;

namespace Pottencial.Payment.UnitTests.Domain.ValueObjects;

public class EmailTest
{

    [Fact]
    public void NovoEmail_ComEnderecoValido_DeveSerValido()
    {
        // Arrange
        var endereco = "teste.teste@teste.com";
        var email = new Email(endereco);

        // Act
        var valido = email.EhValido();

        // Assert
        Assert.True(valido);
        Assert.Equal(endereco, email.Endereco);
    }

    [Theory]
    [InlineData("")]
    [InlineData("teste.teste.com")]
    [InlineData("@teste.com")]
    [InlineData("teste@")]
    [InlineData(".teste@teste.com")]
    public void NovoEmail_ComValorInvalido_DeveTerErros(string endereco)
    {
        // Arrange
        var expected = "O endereco de email precisa ser valido";

        // Act
        var email = new Email(endereco);
        var erros = email.Erros;

        // Assert
        Assert.Equal(expected, erros.FirstOrDefault());
    }
}
