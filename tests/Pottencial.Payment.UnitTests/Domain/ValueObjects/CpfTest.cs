using Pottencial.Payment.Domain.ValueObjects;

namespace Pottencial.Payment.UnitTests.Domain.ValueObjects;

public class CpfTest
{
    [Fact]
    public void NovoCpf_ComNumeroValido_DeveSerValido()
    {
        // Arrange
        var numero = "10987654321";
        var cpf = new Cpf(numero);

        // Act
        var valido = cpf.EhValido();

        // Assert
        Assert.True(valido);
        Assert.Equal(numero, cpf.Numero);
    }

    [Fact]
    public void NovoCpf_ComNumeroValido_DeveRetornarFormatado()
    {
        // Arrange
        var expected = "109.876.543-21";
        var cpf = new Cpf("10987654321");

        // Act
        var cpfFormatado = cpf.CpfFormatado();

        // Assert
        Assert.Equal(expected, cpfFormatado);
    }

    [Theory]
    [InlineData("123456")]
    [InlineData("109.876.543-21")]
    [InlineData("ABCDEFGHIJK")]
    public void NovoCpf_ComValorInvalido_DeveTerErros(string numero)
    {
        // Arrange
        var expected = "O cpf deve possuir 11 digitos e ser valido";

        // Act
        var cpf = new Cpf(numero);
        var erros = cpf.Erros;

        // Assert
        Assert.Equal(expected, erros.FirstOrDefault());
    }

    [Fact]
    public void NovoCpf_ComNumeroValido_DeveRetornarMascarado()
    {
        // Arrange
        var expected = "109.***.***-21";
        var cpf = new Cpf("10987654321");

        // Act
        var cpfMascarado = cpf.CpfMascarado();

        // Assert
        Assert.Equal(expected, cpfMascarado);
    }
}
