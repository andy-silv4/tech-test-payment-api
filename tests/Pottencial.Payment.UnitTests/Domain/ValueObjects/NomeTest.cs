using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.ValueObjects;
using Xunit;

namespace Pottencial.Payment.UnitTests.Domain.ValueObjects;

public class NomeTest
{
    [Fact]
    public void NovoNome_ComNomeESobrenomeValido_DeveSerValido()
    {
        // Arrange
        var primeiroNome = "Nome";
        var sobrenome = "Sobrenome";
        var nome = new Nome(primeiroNome, sobrenome);

        // Act
        var valido = nome.EhValido();

        // Assert
        Assert.True(valido);
        Assert.Equal(primeiroNome, nome.PrimeiroNome);
        Assert.Equal(sobrenome, nome.Sobrenome);
    }

    [Fact]
    public void NovoNome_ComNomeInvalido_DeveTerErros()
    {
        // Arrange
        var expected = "O nome não poder ser vazio ou conter apenas espaços em branco";

        // Act
        var nome = new Nome(string.Empty, "Sobrenome");
        var erros = nome.Erros;

        // Assert
        Assert.Equal(expected, erros.FirstOrDefault());
    }

    [Fact]
    public void NovoNome_ComSobrenomeInvalido_DeveTerErros()
    {
        // Arrange
        var expected = "O nome não poder ser vazio ou conter apenas espaços em branco";

        // Act
        var nome = new Nome("Nome", string.Empty);
        var erros = nome.Erros;

        // Assert
        Assert.Equal(expected, erros.FirstOrDefault());
    }

    [Fact]
    public void NovoNome_ComNomeESobrenomeValido_DeveRetornarNomeCompleto()
    {
        // Arrange
        var expected = "Nome Sobrenome da Silva";
        var nome = new Nome("Nome", "Sobrenome da Silva");

        // Act
        var nomeCompleto = nome.NomeCompleto();

        // Assert
        Assert.Equal(expected, nomeCompleto);
    }
}
