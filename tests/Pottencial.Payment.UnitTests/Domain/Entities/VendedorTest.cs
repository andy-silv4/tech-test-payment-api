using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.UnitTests.Fixtures;

namespace Pottencial.Payment.UnitTests.Domain.Entities;

[Collection(nameof(VendedorCollectionFixture))]
public class VendedorTest
{
    private readonly VendedorFixture _fixture;

    public VendedorTest(VendedorFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void NovoVendedor_ComValoresValidos_DeveSerValido()
    {
        // Arrange
        var vendedor = _fixture.VendedorValido();

        // Act
        var valido = vendedor.EhValido();

        // Assert
        Assert.True(valido);
        Assert.NotEqual(Guid.Empty, vendedor.Id);
        Assert.NotNull(vendedor.Nome);
        Assert.NotNull(vendedor.Cpf);
        Assert.NotNull(vendedor.Email);
        Assert.NotNull(vendedor.Telefone);
    }

    [Fact]
    public void NovoVendedor_ComValoresInvalidos_DeveLancarException()
    {
        // Arrange
        var expected = new List<string>()
        {
            "O nome não poder ser vazio ou conter apenas espaços em branco",
            "O cpf deve possuir 11 digitos e ser valido",
            "O endereco de email precisa ser valido",
            "O ddd deve ter 2 digitos",
            "O telefone deve ter entre 8 e 9 digitos",
            "O telefone deve possuir apenas numeros"
        };

        // Act
        var action = () => _fixture.VendedorInvalido();

        // Assert
        var exception = Assert.Throws<DomainException>(action);
        Assert.Equal(expected, exception.Message.Split("|").ToList());
    }
}
