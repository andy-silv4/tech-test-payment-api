using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.UnitTests.Fixtures;

namespace Pottencial.Payment.UnitTests.Domain.Entities;

[Collection(nameof(VendaCollectionFixture))]
public class VendaTest
{
    private readonly VendaFixture _fixture;

    public VendaTest(VendaFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void NovaVenda_ComValoresValidos_DeveSerValida()
    {
        // Arrange
        var venda = _fixture.VendaValida();

        // Act
        var valido = venda.EhValido();

        // Assert
        Assert.True(valido);

        Assert.NotEqual(Guid.Empty, venda.Id);
        Assert.NotEqual(default(DateTime), venda.DataVenda);
        Assert.NotEqual(Guid.Empty, venda.VendedorId);
        Assert.NotNull(venda.Itens);

        Assert.Equal(StatusVenda.AguardandoPagamento, venda.Status);
    }

    [Fact]
    public void NovaVenda_ComItemValido_DeveAdicionarItem()
    {
        // Arrange
        var venda = _fixture.VendaValida();
        var item = new VendaItem(venda.Id, "P5678", 3, "produto 3", 10, 2.39M);

        // Act
        venda.AdicionarItens(new List<VendaItem>() { item });

        // Assert
        Assert.Equal(3, venda.Itens.Count);
        Assert.Contains(item, venda.Itens);
    }

    [Fact]
    public void NovaVenda_ComItensValidos_DeveTerValorTotalCalculado()
    {
        // Arrange
        var valorTotal = 99.99M;

        var venda = new Venda(vendedorId: Guid.NewGuid());
        var itens = new List<VendaItem>()
        {
            new VendaItem(venda.Id, "P1234", 1, "produto 1", 1, 53.79M),
            new VendaItem(venda.Id, "P5678", 2,  "produto 2", 3, 15.40M),
        };

        // Act
        venda.AdicionarItens(itens).CalcularValorTotal();

        // Assert
        Assert.Equal(valorTotal, venda.ValorTotal);
    }

    [Fact]
    public void NovaVenda_ComIdVendedorInvalido_DeveLancarException()
    {
        // Arrange
        var expected = new List<string>()
        {
            "A venda deve ter um identificador valido para o 'VendedorId'"
        };

        // Act
        var action = () => _fixture.VendaInvalida();

        // Assert
        var exception = Assert.Throws<DomainException>(action);
        Assert.Equal(expected, exception.Message.Split("|").ToList());
    }

    [Fact]
    public void NovaVenda_SemItensAdicionados_DeveLancarException()
    {
        // Arrange
        var expected = new List<string>()
        {
            "A venda deve ter no minimo 1 item"
        };

        // Act
        var action = () => _fixture.VendaInvalidaSemItens().CalcularValorTotal();

        // Assert
        var exception = Assert.Throws<DomainException>(action);
        Assert.Equal(expected, exception.Message.Split("|").ToList());
    }

    [Fact]
    public void NovaVenda_ComItensIdVendaDiferente_DeveLancarException()
    {
        // Arrange
        var expected = "O item deve ter o mesmo identificador da venda";

        var vendaId = Guid.NewGuid();
        var itens = new List<VendaItem>()
        {
            new VendaItem(vendaId, "P1234", 1, "produto 1", 1, 53.79M),
            new VendaItem(vendaId, "P5678", 2,  "produto 2", 3, 15.40M),
        };

        var venda = new Venda(vendedorId: Guid.NewGuid());

        // Act
        var action = () => venda.AdicionarItens(itens);

        // Assert
        var exception = Assert.Throws<DomainException>(action);
        Assert.Equal(expected, exception.Message);
    }
}
