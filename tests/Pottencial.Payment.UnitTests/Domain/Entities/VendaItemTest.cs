using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;

namespace Pottencial.Payment.UnitTests.Domain.Entities;

public class VendaItemTest
{
    [Fact]
    public void NovoVendaItem_ComValoresValidos_DeveSerValido()
    {
        // Arrange
        var vendaItem = new VendaItem(Guid.NewGuid(), "P1234", 1, "produto 1", 3, 25.52M);

        // Act
        var valido = vendaItem.EhValido();

        // Assert
        Assert.True(valido);
        Assert.NotNull(vendaItem.ProdutoId);
        Assert.NotNull(vendaItem.NomeProduto);
        Assert.NotEqual(Guid.Empty, vendaItem.Id);
        Assert.NotEqual(Guid.Empty, vendaItem.VendaId);
        Assert.NotEqual(default(int), vendaItem.Quantidade);
        Assert.NotEqual(default(decimal), vendaItem.ValorUnidade);
    }

    [Theory]
    [InlineData(1, 0.1, 0.1)]
    [InlineData(3, 25.52, 76.56)]
    public void NovaVendaItem_ComItensValidos_DeveTerValorTotalCalculado(int quantidade, decimal valor,
                                                                         decimal valorEsperado)
    {
        // Arrange
        var venda = new VendaItem(Guid.NewGuid(), "P1234", 1, "produto 1", quantidade, valor);

        // Act
        var valorTotal = venda.ValorTotal();

        // Assert
        Assert.Equal(valorEsperado, valorTotal);
    }

    [Fact]
    public void NovoVendaItem_ComValoresInvalidos_DeveLancarException()
    {
        // Arrange
        var expected = new List<string>()
        {
            $"O item deve ter um identificador de uma venda existente" ,
            $"O item deve ter o identificador do produto" ,
            $"O item deve ter o nome do produto",
            $"A quantidade do produto deve ser no minimo {VendaItem.QuantidadeMinima}",
            $"O valor do produto deve ser no minimo {VendaItem.ValorMinimo}"
        };

        // Act
        var action = () => new VendaItem(default(Guid), string.Empty, 0, string.Empty, 0, 0.00M);

        // Assert
        var exception = Assert.Throws<DomainException>(action);
        Assert.Equal(expected, exception.Message.Split("|").ToList());
    }
}
