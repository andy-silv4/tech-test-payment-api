using Moq;
using Moq.AutoMock;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Domain.Services;
using Pottencial.Payment.UnitTests.Fixtures;

namespace Pottencial.Payment.UnitTests.Domain.Services;

[Collection(nameof(VendaCollectionFixture))]
public class StatusVendaServiceTest
{
    private readonly VendaFixture _fixture;
    private readonly AutoMocker _mocker;

    private readonly IStatusVendaService _service;

    public StatusVendaServiceTest(VendaFixture fixture)
    {
        _fixture = fixture;

        _mocker = new AutoMocker();
        _service = _mocker.CreateInstance<StatusVendaService>();
    }

    [Theory]
    [InlineData(StatusVenda.PagamentoAprovado)]
    [InlineData(StatusVenda.Cancelado)]
    public void StatusStatusVendaService_VendaAguardandoPagamento_DeveAtualizarStatus(StatusVenda statusVenda)
    {
        // Arrange
        var venda = _fixture.VendaValidaAguardandoPagamento();
        var id = venda.Id;

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(venda);

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.Commit())
            .ReturnsAsync(true);

        // Act
        var atualizado = _service.AtualizarStatus(id, statusVenda).Result;

        // Assert
        Assert.True(atualizado);
        Assert.Equal(statusVenda, venda.Status);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Once);
    }

    [Theory]
    [InlineData(StatusVenda.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.Cancelado)]
    public void StatusVendaService_VendaComPagamentoAprovado_DeveAtualizarStatus(StatusVenda statusVenda)
    {
        // Arrange
        var venda = _fixture.VendaValidaComPagamentoAprovado();
        var id = venda.Id;

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(venda);

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.Commit())
            .ReturnsAsync(true);

        // Act
        var atualizado = _service.AtualizarStatus(id, statusVenda).Result;

        // Assert
        Assert.True(atualizado);
        Assert.Equal(statusVenda, venda.Status);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Once);
    }

    [Theory]
    [InlineData(StatusVenda.Entregue)]
    public void StatusVendaService_VendaEnviadoParaTransportadora_DeveAtualizarStatus(StatusVenda statusVenda)
    {
        // Arrange
        var venda = _fixture.VendaValidaEnviadoParaTransportadora();
        var id = venda.Id;

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(venda);

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.Commit())
            .ReturnsAsync(true);

        // Act
        var atualizado = _service.AtualizarStatus(id, statusVenda).Result;

        // Assert
        Assert.True(atualizado);
        Assert.Equal(statusVenda, venda.Status);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Once);
    }

    [Fact]
    public void StatusVendaService_VendaNaoExistente_NaoDeveAtualizar()
    {
        // Arrange
        var id = Guid.NewGuid();

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(default(Venda)!);

        // Act
        var atualizado = _service.AtualizarStatus(id, StatusVenda.Entregue).Result;

        // Assert
        Assert.False(atualizado);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Never);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Never);
    }

    [Theory]
    [InlineData(StatusVenda.AguardandoPagamento)]
    [InlineData(StatusVenda.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.Entregue)]
    public void StatusStatusVendaService_VendaAguardandoPagamento_DeveLancarException(StatusVenda statusVenda)
    {
        // Arrange
        var venda = _fixture.VendaValidaAguardandoPagamento();
        var id = venda.Id;
        var expected = $"Não é possível alterar o status de '{venda.Status}' para '{statusVenda}'";

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(venda);

        // Act
        var action = () => _service.AtualizarStatus(id, statusVenda);

        // Assert
        var exception = Assert.ThrowsAsync<DomainException>(action);
        Assert.Equal(expected, exception.Result.Message);
        Assert.Equal(StatusVenda.AguardandoPagamento, venda.Status);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Never);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Never);
    }

    [Theory]
    [InlineData(StatusVenda.PagamentoAprovado)]
    [InlineData(StatusVenda.Entregue)]
    public void StatusVendaService_VendaComPagamentoAprovado_DeveLancarException(StatusVenda statusVenda)
    {
        // Arrange
        var venda = _fixture.VendaValidaComPagamentoAprovado();
        var id = venda.Id;
        var expected = $"Não é possível alterar o status de '{venda.Status}' para '{statusVenda}'";

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(venda);

        // Act
        var action = () => _service.AtualizarStatus(id, statusVenda);

        // Assert
        var exception = Assert.ThrowsAsync<DomainException>(action);
        Assert.Equal(expected, exception.Result.Message);
        Assert.Equal(StatusVenda.PagamentoAprovado, venda.Status);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Never);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Never);
    }

    [Theory]
    [InlineData(StatusVenda.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.PagamentoAprovado)]
    public void StatusVendaService_VendaEnviadoParaTransportadora_DeveLancarException(StatusVenda statusVenda)
    {
        // Arrange
        var venda = _fixture.VendaValidaEnviadoParaTransportadora();
        var id = venda.Id;
        var expected = $"Não é possível alterar o status de '{venda.Status}' para '{statusVenda}'";

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(venda);

        // Act
        var action = () => _service.AtualizarStatus(id, statusVenda);

        // Assert
        var exception = Assert.ThrowsAsync<DomainException>(action);
        Assert.Equal(expected, exception.Result.Message);
        Assert.Equal(StatusVenda.EnviadoParaTransportadora, venda.Status);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Never);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Never);
    }

    [Fact]
    public void StatusVendaService_CancelarEnviadoParaTransportadora_DeveLancarException()
    {
        // Arrange
        var venda = _fixture.VendaValidaEnviadoParaTransportadora();
        var id = venda.Id;
        var expected = $"Não é possível cancelar a venda com o status '{venda.Status}'";

        _mocker.GetMock<IUnitOfWork>().Setup(x => x.VendaRepository.BuscarVenda(id))
            .ReturnsAsync(venda);

        // Act
        var action = () => _service.AtualizarStatus(id, StatusVenda.Cancelado);

        // Assert
        var exception = Assert.ThrowsAsync<DomainException>(action);
        Assert.Equal(expected, exception.Result.Message);
        Assert.Equal(StatusVenda.EnviadoParaTransportadora, venda.Status);

        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.BuscarVenda(It.IsAny<Guid>()), Times.Once);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.VendaRepository.AtualizarVenda(It.IsAny<Venda>()), Times.Never);
        _mocker.GetMock<IUnitOfWork>().Verify(x => x.Commit(), Times.Never);
    }
}
