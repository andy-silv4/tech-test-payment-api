using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;

namespace Pottencial.Payment.UnitTests.Fixtures;

[CollectionDefinition(nameof(VendaCollectionFixture))]
public class VendaCollectionFixture : ICollectionFixture<VendaFixture> { }

public class VendaFixture
{
    public Venda VendaValida()
    {
        var venda = new Venda(vendedorId: Guid.NewGuid());

        var itens = new List<VendaItem>()
        {
            new VendaItem(venda.Id, "P1234", 1, "produto 1", 1, 25.52M),
            new VendaItem(venda.Id, "P5678", 2,  "produto 2", 2, 9.99M),
        };

        venda.AdicionarItens(itens);

        return venda;
    }

    public Venda VendaValidaAguardandoPagamento()
    {
        return VendaValida();
    }

    public Venda VendaValidaComPagamentoAprovado()
    {
        var venda = VendaValidaAguardandoPagamento();

        venda.AtualizarStatus(StatusVenda.PagamentoAprovado);

        return venda;
    }

    public Venda VendaValidaEnviadoParaTransportadora()
    {
        var venda = VendaValidaComPagamentoAprovado();

        venda.AtualizarStatus(StatusVenda.EnviadoParaTransportadora);

        return venda;
    }

    public Venda VendaInvalida()
    {
        return new Venda(Guid.Empty);
    }

    public Venda VendaInvalidaSemItens()
    {
        var vendaId = Guid.NewGuid();

        var venda = new Venda(Guid.NewGuid());

        return venda;
    }
}
