using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.ValueObjects;

namespace Pottencial.Payment.UnitTests.Fixtures;

[CollectionDefinition(nameof(VendedorCollectionFixture))]
public class VendedorCollectionFixture : ICollectionFixture<VendedorFixture> { }

public class VendedorFixture
{
    public Vendedor VendedorValido()
    {
        return new Vendedor(new Nome("Nome", "Sobrenome"),
                            new Cpf("12345678910"),
                            new Email("teste@teste.com"),
                            new Telefone("10", "987654321"));
    }

    public Vendedor VendedorInvalido()
    {
        return new Vendedor(new Nome(string.Empty, string.Empty),
                            new Cpf(string.Empty),
                            new Email(string.Empty),
                            new Telefone(string.Empty, string.Empty));
    }
}
