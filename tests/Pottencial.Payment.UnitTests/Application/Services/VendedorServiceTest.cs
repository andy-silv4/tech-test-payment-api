using Moq;
using Moq.AutoMock;
using Pottencial.Payment.API.Application.Services;
using Pottencial.Payment.API.Application.V1.Mappers;
using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Application.V1.Services;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.Domain.ValueObjects;
using Pottencial.Payment.UnitTests.Fixtures;

namespace Pottencial.Payment.UnitTests.Application.Services;

[Collection(nameof(VendaCollectionFixture))]
public class VendedorServiceTest
{
    private readonly AutoMocker _mocker;
    private readonly IVendedorService _service;

    public VendedorServiceTest()
    {
        _mocker = new AutoMocker();
        _service = _mocker.CreateInstance<VendedorService>();
    }

    [Fact]
    public void VendedorService_AdicionarVendedor_DeveAdicionarVendedor()
    {
        // Arrange
        var request = new VendedorRequest("10987654321", "Nome", "Sobrenome", "testem@email.com", "31", "987654321");

        _mocker.GetMock<IVendedorRepository>().Setup(x => x.BuscarVendedor(It.IsAny<Cpf>()))
            .ReturnsAsync((Vendedor)null!);

        // Act
        var response = _service.AdicionarVendedor(request).Result;

        // Assert
        Assert.NotNull(response);
        _mocker.GetMock<IVendedorRepository>().Verify(x => x.BuscarVendedor(It.IsAny<Cpf>()), Times.Once);
        _mocker.GetMock<IVendedorRepository>().Verify(x => x.AdicionarVendedor(It.IsAny<Vendedor>()), Times.Once);
    }

    [Fact]
    public void VendedorService_AdicionarVendedor_NaoDeveAdicionarVendedor()
    {
        // Arrange
        var request = new VendedorRequest("10987654321", "Nome", "Sobrenome", "testem@email.com", "31", "987654321");
        var vendedor = request.ParaEntity();

        _mocker.GetMock<IVendedorRepository>().Setup(x => x.BuscarVendedor(It.IsAny<Cpf>()))
            .ReturnsAsync(vendedor);

        // Act
        var response = _service.AdicionarVendedor(request).Result;

        // Assert
        Assert.NotNull(response);
        _mocker.GetMock<IVendedorRepository>().Verify(x => x.BuscarVendedor(It.IsAny<Cpf>()), Times.Once);
        _mocker.GetMock<IVendedorRepository>().Verify(x => x.AdicionarVendedor(It.IsAny<Vendedor>()), Times.Never);
    }
}