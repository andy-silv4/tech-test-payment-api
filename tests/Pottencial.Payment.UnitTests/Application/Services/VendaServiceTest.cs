using Moq;
using Moq.AutoMock;
using Pottencial.Payment.API.Application.Services;
using Pottencial.Payment.API.Application.V1.Mappers;
using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Application.V1.Services;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Interfaces;
using Pottencial.Payment.UnitTests.Fixtures;

namespace Pottencial.Payment.UnitTests.Application.Services;

[Collection(nameof(VendaCollectionFixture))]
public class VendaServiceTest
{
    private readonly AutoMocker _mocker;
    private readonly IVendaService _service;

    public VendaServiceTest()
    {
        _mocker = new AutoMocker();
        _service = _mocker.CreateInstance<VendaService>();
    }

    [Fact]
    public void VendaService_AdicionarVenda_DeveAdicionarVenda()
    {
        // Arrange
        var vendedorRequest = new VendedorRequest("10987654321", "Nome", "Sobrenome", "testem@email.com", "31", "987654321");
        var itens = new List<ItemRequest>()
        {
            new ItemRequest("P1234", "produto 1", 1, 25.52M),
            new ItemRequest("P5678", "produto 2", 2, 9.99M),
        };

        var request = new VendaRequest(vendedorRequest, itens);

        _mocker.GetMock<IVendedorService>().Setup(x => x.AdicionarVendedor(vendedorRequest))
            .ReturnsAsync(vendedorRequest.ParaEntity().ParaOutput());

        // Act
        var response = _service.AdicionarVenda(request).Result;

        // Assert
        Assert.NotNull(response);
        _mocker.GetMock<IVendedorService>().Verify(x => x.AdicionarVendedor(It.IsAny<VendedorRequest>()), Times.Once);
        _mocker.GetMock<IVendaRepository>().Verify(x => x.AdicionarVenda(It.IsAny<Venda>()), Times.Once);
    }

    [Fact]
    public void VendaService_AtualizarStatus_DeveAtualizarStatus()
    {
        // Arrange
        var status = StatusVenda.Cancelado;

        _mocker.GetMock<IStatusVendaService>().Setup(x => x.AtualizarStatus(It.IsAny<Guid>(), status))
            .ReturnsAsync(true);

        // Act
        var atualizado = _service.AtualizarStatusVenda(Guid.NewGuid(), (VendaStatus)status).Result;

        // Assert
        Assert.True(atualizado);
        _mocker.GetMock<IStatusVendaService>().Verify(x => x.AtualizarStatus(It.IsAny<Guid>(), status), Times.Once);
        _mocker.GetMock<IVendaRepository>().Verify(x => x.Commit(), Times.Once);
    }

    [Fact]
    public void VendaService_AtualizarStatus_NaoDeveAtualizarStatus()
    {
        // Arrange
        var status = StatusVenda.Cancelado;

        _mocker.GetMock<IStatusVendaService>().Setup(x => x.AtualizarStatus(It.IsAny<Guid>(), status))
            .ReturnsAsync(false);

        // Act
        var atualizado = _service.AtualizarStatusVenda(Guid.NewGuid(), (VendaStatus)status).Result;

        // Assert
        Assert.False(atualizado);
        _mocker.GetMock<IStatusVendaService>().Verify(x => x.AtualizarStatus(It.IsAny<Guid>(), status), Times.Once);
        _mocker.GetMock<IVendaRepository>().Verify(x => x.Commit(), Times.Never);
    }
}
