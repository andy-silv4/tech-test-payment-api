using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Infra.Context;

namespace Pottencial.Payment.IntegrationTests.Fixtures;

[CollectionDefinition(nameof(IntegrationTestFixtureCollection))]
public class IntegrationTestFixtureCollection : ICollectionFixture<IntegrationTestFixture<Program>> { }

public class IntegrationTestFixture<TStartup> : IDisposable where TStartup : class
{
    public HttpClient Client;
    public readonly WebApplicationFactory<TStartup> Factory;
    public VendedorFixture VendedorFixture { get; set; } = null!;
    public VendaFixture VendaFixture { get; set; } = null!;

    public IntegrationTestFixture()
    {
        VendaFixture = new VendaFixture();
        VendedorFixture = new VendedorFixture();

        Factory = new CustomWebApplicationFactory<TStartup>();
        Client = Factory.CreateClient(new WebApplicationFactoryClientOptions { });
    }

    public IList<Guid> InserirVendas(StatusVenda status = StatusVenda.AguardandoPagamento)
    {
        var vendedores = VendedorFixture.InserirVendedores();
        var vendedorId = vendedores.First().Id;

        var vendas = VendaFixture.CriarVendas(vendedorId, status);

        Client = Factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var context = scopedServices.GetRequiredService<ApplicationDbContext>();

                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();

                    context.Vendedores.AddRange(vendedores);
                    context.Vendas.AddRange(vendas);

                    context.SaveChanges();
                }
            });
        })
        .CreateClient(new WebApplicationFactoryClientOptions { });

        return vendas.Select(x => x.Id).ToList();
    }

    public IList<Vendedor> InserirVendedores()
    {
        var vendedores = VendedorFixture.InserirVendedores();

        Client = Factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var context = scopedServices.GetRequiredService<ApplicationDbContext>();

                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();

                    context.Vendedores.AddRange(vendedores);

                    context.SaveChanges();
                }
            });
        })
        .CreateClient(new WebApplicationFactoryClientOptions { });

        return vendedores;
    }

    public void Dispose()
    {
        Client.Dispose();
        Factory.Dispose();
    }
}
