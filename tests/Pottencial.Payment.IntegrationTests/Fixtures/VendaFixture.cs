using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda;
using ItemRequest = Pottencial.Payment.Application.V1.DTOs.ItemRequest;
using ItemRequestV2 = Pottencial.Payment.Application.V2.DTOs.ItemRequest;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enums;

namespace Pottencial.Payment.IntegrationTests.Fixtures;

[CollectionDefinition(nameof(VendaFixtureCollection))]
public class VendaFixtureCollection : ICollectionFixture<VendaFixture> { }

public class VendaFixture
{
    public Venda VendaValida(Guid vendedorId)
    {
        var venda = new Venda(vendedorId);

        var itens = new List<VendaItem>()
        {
            new VendaItem(venda.Id, "P1234", 1, "produto 1", 1, 25.52M),
            new VendaItem(venda.Id, "P5678", 2,  "produto 2", 2, 9.99M),
        };

        venda.AdicionarItens(itens).CalcularValorTotal();

        return venda;
    }

    public Venda VendaValidaAguardandoPagamento(Guid vendedorId)
    {
        return VendaValida(vendedorId);
    }

    public Venda VendaValidaComPagamentoAprovado(Guid vendedorId)
    {
        var venda = VendaValidaAguardandoPagamento(vendedorId);

        venda.AtualizarStatus(StatusVenda.PagamentoAprovado);

        return venda;
    }

    public Venda VendaValidaEnviadoParaTransportadora(Guid vendedorId)
    {
        var venda = VendaValidaComPagamentoAprovado(vendedorId);

        venda.AtualizarStatus(StatusVenda.EnviadoParaTransportadora);

        return venda;
    }

    public List<Venda> CriarVendas(Guid vendedorId, StatusVenda status, int quantidade = 1)
    {
        var vendas = new List<Venda>();

        for (int i = 0; i < quantidade; i++)
        {
            switch (status)
            {
                case StatusVenda.PagamentoAprovado:
                    vendas.Add(VendaValidaComPagamentoAprovado(vendedorId));
                    break;
                case StatusVenda.EnviadoParaTransportadora:
                    vendas.Add(VendaValidaEnviadoParaTransportadora(vendedorId));
                    break;
                default:
                    vendas.Add(VendaValidaAguardandoPagamento(vendedorId));
                    break;
            }
        }

        return vendas;
    }

    public VendaRequest VendaRequestValida()
    {
        var vendedor = new VendedorRequest("10987654321", "Nome", "Sobrenome",
                                           "testem@email.com", "31", "987654321");
        var itens = new List<ItemRequest>()
        {
            new ItemRequest("P1234", "produto 1", 1, 25.52M),
            new ItemRequest("P5678", "produto 2", 2, 9.99M),
        };

        return new VendaRequest(vendedor, itens);
    }

    public VendaRequest VendaRequestInvalida()
    {
        var vendedor = new VendedorRequest(string.Empty, string.Empty, string.Empty,
                                           string.Empty, string.Empty, string.Empty);
        var itens = new List<ItemRequest>()
        {
            new ItemRequest(string.Empty, string.Empty, 0, 0M),
            new ItemRequest(string.Empty, string.Empty, 0, 0M),
        };

        return new VendaRequest(vendedor, itens);
    }

    public RegistrarVendaCommand RegistrarVendaCommandValido(Guid vendedorId)
    {
        var itens = new List<ItemRequestV2>()
        {
            new ItemRequestV2("P1234", "produto 1", 1, 25.52M),
            new ItemRequestV2("P5678", "produto 2", 2, 9.99M),
        };

        return new RegistrarVendaCommand(vendedorId, itens);
    }

    public RegistrarVendaCommand RegistrarVendaCommandInvalido()
    {
        var itens = new List<ItemRequestV2>();

        return new RegistrarVendaCommand(Guid.NewGuid(), itens);
    }
}
