using Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.ValueObjects;

namespace Pottencial.Payment.IntegrationTests.Fixtures;

public class VendedorFixture
{
    public Vendedor VendedorValido()
    {
        return new Vendedor(new Nome("Nome", "Sobrenome"),
                            new Cpf("12345678910"),
                            new Email("teste@teste.com"),
                            new Telefone("10", "987654321"));
    }

    public List<Vendedor> InserirVendedores()
    {
        var vendedor = new List<Vendedor>();
        vendedor.Add(VendedorValido());

        return vendedor;
    }

    public CadastrarVendedorCommand CadastrarVendedorCommandValido()
    {
        var vendedor = VendedorValido();

        return new CadastrarVendedorCommand(
            cpf: "10987654321",
            vendedor.Nome.PrimeiroNome,
            vendedor.Nome.Sobrenome,
            email: "teste@email.com",
            vendedor.Telefone.Ddd,
            vendedor.Telefone.Numero
        );
    }

    public CadastrarVendedorCommand CadastrarVendedorCommandInvalido()
    {
        return new CadastrarVendedorCommand(
            cpf: string.Empty,
            nome: string.Empty,
            sobrenome: string.Empty,
            email: string.Empty,
            ddd: string.Empty,
            telefone: string.Empty
        );
    }
}
