using System.Net;
using System.Net.Http.Json;
using Newtonsoft.Json;
using Pottencial.Payment.Application.V2.Enums;
using Pottencial.Payment.Application.V2.UseCases.Vendas.AtualizarStatusVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.BuscarVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendas.RegistrarVenda;
using Pottencial.Payment.Application.V2.UseCases.Vendedores.BuscarVendedor;
using Pottencial.Payment.Application.V2.UseCases.Vendedores.CadastrarVendedor;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.IntegrationTests.Fixtures;

namespace Pottencial.Payment.IntegrationTests.API.V2;

[Collection(nameof(IntegrationTestFixtureCollection))]
public class ApiTest
{
    private const string BaseApi = "/api/v2";

    private readonly HttpClient _client;
    private readonly IntegrationTestFixture<Program> _fixture;

    public ApiTest(IntegrationTestFixture<Program> fixture)
    {
        _fixture = fixture;
        _client = _fixture.Client;
    }

    [Fact]
    public async Task Get_EndpointBuscarVenda_DeveRetornarSucesso()
    {
        // Arrange
        var id = _fixture.InserirVendas().FirstOrDefault();

        // Act
        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(id, getResponse.Id);
        Assert.Equal(HttpStatusCode.OK, getResult.StatusCode);
    }

    [Fact]
    public async Task Get_EndpointBuscarVenda_DeveRetornarNaoEncontrado()
    {
        // Arrange
        var id = Guid.NewGuid();

        // Act
        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(Guid.Empty, getResponse.Id);
        Assert.Equal(HttpStatusCode.NotFound, getResult.StatusCode);
    }

    [Fact]
    public async Task Post_EndpointRegistrarVenda_DeveRetornarSucesso()
    {
        // Arrange
        var vendedor = _fixture.InserirVendedores().FirstOrDefault();
        var command = _fixture.VendaFixture.RegistrarVendaCommandValido(vendedor!.Id);

        // Act
        var postResult = await _client.PostAsJsonAsync($"{BaseApi}/vendas", command);
        var postResponse = JsonConvert.DeserializeObject<VendaCommandResult>(postResult.Content.ReadAsStringAsync().Result);

        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{postResponse.Id}");
        var getResponse = JsonConvert.DeserializeObject<VendaQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(HttpStatusCode.Created, postResult.StatusCode);
        Assert.Equal(postResponse.Id, getResponse.Id);
        Assert.Equal(postResponse.DataVenda, getResponse.DataVenda);
    }

    [Fact]
    public async Task Post_EndpointRegistrarVenda_DeveRetornarErros()
    {
        // Arrange
        var request = _fixture.VendaFixture.RegistrarVendaCommandInvalido();

        // Act
        var postResult = await _client.PostAsJsonAsync($"{BaseApi}/vendas", request);
        var response = JsonConvert.DeserializeAnonymousType(postResult.Content.ReadAsStringAsync().Result,
            new { Erros = new List<string>() });

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, postResult.StatusCode);
        Assert.NotEmpty(response.Erros);
    }

    [Fact]
    public async Task Patch_EndpointAtualizarStatusVenda_DeveRetornarNaoEncontrado()
    {
        // Arrange
        var id = Guid.NewGuid();
        var status = new AtualizarStatusVendaCommand(VendaStatus.Cancelado);

        // Act
        var content = JsonContent.Create(status);
        var putResult = await _client.PatchAsync($"{BaseApi}/vendas/{id}", content);

        // Assert
        Assert.Equal(HttpStatusCode.NotFound, putResult.StatusCode);
    }

    [Theory]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.PagamentoAprovado)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.Entregue)]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.Cancelado)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.Cancelado)]
    public async Task Patch_EndpointAtualizarStatusVenda_DeveRetornarSucessoSemConteudo(StatusVenda deStatus, VendaStatus paraStatus)
    {
        // Arrange
        var id = _fixture.InserirVendas(deStatus).FirstOrDefault();
        var status = new AtualizarStatusVendaCommand(paraStatus);

        // Act
        var content = JsonContent.Create(status);
        var putResult = await _client.PatchAsync($"{BaseApi}/vendas/{id}", content);

        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(HttpStatusCode.NoContent, putResult.StatusCode);
        Assert.Equal(status.VendaStatus.ToString(), getResponse.VendaStatus);
    }

    [Theory]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.AguardandoPagamento)]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.Entregue)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.PagamentoAprovado)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.Entregue)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.AguardandoPagamento)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.PagamentoAprovado)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.Cancelado)]
    public async Task Patch_EndpointAtualizarStatusVenda_DeveRetornarErros(StatusVenda deStatus, VendaStatus paraStatus)
    {
        // Arrange
        var id = _fixture.InserirVendas(deStatus).FirstOrDefault();
        var status = new AtualizarStatusVendaCommand(paraStatus);

        // Act
        var content = JsonContent.Create(status);
        var putResult = await _client.PatchAsync($"{BaseApi}/vendas/{id}", content);
        var putResponse = JsonConvert.DeserializeAnonymousType(putResult.Content.ReadAsStringAsync().Result,
            new { Erros = new List<string>() });

        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, putResult.StatusCode);
        Assert.NotEmpty(putResponse.Erros);
        Assert.Equal(deStatus.ToString(), getResponse.VendaStatus);
    }

    [Fact]
    public async Task Get_EndpointBuscarVendedor_DeveRetornarSucesso()
    {
        // Arrange
        var vendedor = _fixture.InserirVendedores().FirstOrDefault();
        var email = vendedor!.Email.Endereco;

        // Act
        var getResult = await _client.GetAsync($"{BaseApi}/vendedores/{email}");
        var getResponse = JsonConvert.DeserializeObject<VendedorQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(email, getResponse.Email);
        Assert.Equal(HttpStatusCode.OK, getResult.StatusCode);
    }

    [Fact]
    public async Task Get_EndpointBuscarVendedorPorId_DeveRetornarNaoEncontrado()
    {
        // Arrange
        var id = Guid.NewGuid();

        // Act
        var getResult = await _client.GetAsync($"{BaseApi}/vendedores/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendedorQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(Guid.Empty, getResponse.Id);
        Assert.Equal(HttpStatusCode.NotFound, getResult.StatusCode);
    }

    [Fact]
    public async Task Get_EndpointBuscarVendedorPorEmail_DeveRetornarNaoEncontrado()
    {
        // Arrange
        var email = "nao.existe@teste.com";

        // Act
        var getResult = await _client.GetAsync($"{BaseApi}/vendedores/{email}");
        var getResponse = JsonConvert.DeserializeObject<VendedorQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.NotEqual(email, getResponse.Email);
        Assert.Equal(HttpStatusCode.NotFound, getResult.StatusCode);
    }

    [Fact]
    public async Task Post_EndpointCadastrarVendedor_DeveRetornarSucesso()
    {
        // Arrange
        var command = _fixture.VendedorFixture.CadastrarVendedorCommandValido();

        // Act
        var postResult = await _client.PostAsJsonAsync($"{BaseApi}/vendedores", command);
        var postResponse = JsonConvert.DeserializeObject<VendedorCommandResult>(postResult.Content.ReadAsStringAsync().Result);

        var getResult = await _client.GetAsync($"{BaseApi}/vendedores/{postResponse.Id}");
        var getResponse = JsonConvert.DeserializeObject<VendedorQueryResult>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(HttpStatusCode.Created, postResult.StatusCode);
        Assert.Equal(postResponse.Id, getResponse.Id);
        Assert.Equal(postResponse.Cpf, getResponse.Cpf);
        Assert.Equal(postResponse.Email, getResponse.Email);
        Assert.Equal(command.Email, getResponse.Email);
    }

    [Fact]
    public async Task Post_EndpointCadastrarVendedor_DeveRetornarErros()
    {
        // Arrange
        var command = _fixture.VendedorFixture.CadastrarVendedorCommandInvalido();

        // Act
        var postResult = await _client.PostAsJsonAsync($"{BaseApi}/vendedores", command);
        var response = JsonConvert.DeserializeAnonymousType(postResult.Content.ReadAsStringAsync().Result,
            new { Erros = new List<string>() });

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, postResult.StatusCode);
        Assert.NotEmpty(response.Erros);
    }
}
