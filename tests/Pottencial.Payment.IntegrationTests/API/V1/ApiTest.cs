using System.Net;
using System.Net.Http.Json;
using Newtonsoft.Json;
using Pottencial.Payment.Application.V1.DTOs;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.IntegrationTests.Fixtures;

namespace Pottencial.Payment.IntegrationTests.API.V1;

[Collection(nameof(IntegrationTestFixtureCollection))]
public class ApiTest
{
    private const string BaseApi = "/api/v1";

    private readonly HttpClient _client;
    private readonly IntegrationTestFixture<Program> _fixture;

    public ApiTest(IntegrationTestFixture<Program> fixture)
    {
        _fixture = fixture;
        _client = _fixture.Client;
    }

    [Fact]
    public async Task Get_EndpointBuscar_DeveRetornarSucesso()
    {
        // Arrange
        var id = _fixture.InserirVendas().FirstOrDefault();

        // Act
        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaResponse>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(id, getResponse.Id);
        Assert.Equal(HttpStatusCode.OK, getResult.StatusCode);
    }

    [Fact]
    public async Task Get_EndpointBuscar_DeveRetornarNaoEncontrado()
    {
        // Arrange
        var id = Guid.NewGuid();

        // Act
        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaResponse>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(Guid.Empty, getResponse.Id);
        Assert.Equal(HttpStatusCode.NotFound, getResult.StatusCode);
    }

    [Fact]
    public async Task Post_EndpointRegistrar_DeveRetornarSucesso()
    {
        // Arrange
        var request = _fixture.VendaFixture.VendaRequestValida();

        // Act
        var postResult = await _client.PostAsJsonAsync($"{BaseApi}/vendas", request);
        var postResponse = JsonConvert.DeserializeObject<VendaResponse>(postResult.Content.ReadAsStringAsync().Result);

        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{postResponse.Id}");
        var getResponse = JsonConvert.DeserializeObject<VendaResponse>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(HttpStatusCode.Created, postResult.StatusCode);
        Assert.Equal(postResponse, getResponse);
    }

    [Fact]
    public async Task Post_EndpointRegistrar_DeveRetornarErros()
    {
        // Arrange
        var request = _fixture.VendaFixture.VendaRequestInvalida();

        // Act
        var postResult = await _client.PostAsJsonAsync($"{BaseApi}/vendas", request);
        var response = JsonConvert.DeserializeAnonymousType(postResult.Content.ReadAsStringAsync().Result,
            new { Erros = new List<string>() });

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, postResult.StatusCode);
        Assert.NotEmpty(response.Erros);
    }

    [Fact]
    public async Task Patch_EndpointAtualizarStatus_DeveRetornarNaoEncontrado()
    {
        // Arrange
        var id = Guid.NewGuid();
        var status = new StatusRequest(VendaStatus.Cancelado);

        // Act
        var content = JsonContent.Create(status);
        var putResult = await _client.PatchAsync($"{BaseApi}/vendas/{id}", content);

        // Assert
        Assert.Equal(HttpStatusCode.NotFound, putResult.StatusCode);
    }

    [Theory]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.PagamentoAprovado)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.Entregue)]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.Cancelado)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.Cancelado)]
    public async Task Patch_EndpointAtualizarStatus_DeveRetornarSucessoSemConteudo(StatusVenda deStatus, VendaStatus paraStatus)
    {
        // Arrange
        var id = _fixture.InserirVendas(deStatus).FirstOrDefault();
        var status = new StatusRequest(paraStatus);

        // Act
        var content = JsonContent.Create(status);
        var putResult = await _client.PatchAsync($"{BaseApi}/vendas/{id}", content);

        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaResponse>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(HttpStatusCode.NoContent, putResult.StatusCode);
        Assert.Equal(status.VendaStatus.ToString(), getResponse.VendaStatus);
    }

    [Theory]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.AguardandoPagamento)]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.AguardandoPagamento, VendaStatus.Entregue)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.PagamentoAprovado)]
    [InlineData(StatusVenda.PagamentoAprovado, VendaStatus.Entregue)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.EnviadoParaTransportadora)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.AguardandoPagamento)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.PagamentoAprovado)]
    [InlineData(StatusVenda.EnviadoParaTransportadora, VendaStatus.Cancelado)]
    public async Task Patch_EndpointAtualizarStatus_DeveRetornarErros(StatusVenda deStatus, VendaStatus paraStatus)
    {
        // Arrange
        var id = _fixture.InserirVendas(deStatus).FirstOrDefault();
        var status = new StatusRequest(paraStatus);

        // Act
        var content = JsonContent.Create(status);
        var putResult = await _client.PatchAsync($"{BaseApi}/vendas/{id}", content);
        var putResponse = JsonConvert.DeserializeAnonymousType(putResult.Content.ReadAsStringAsync().Result,
            new { Erros = new List<string>() });

        var getResult = await _client.GetAsync($"{BaseApi}/vendas/{id}");
        var getResponse = JsonConvert.DeserializeObject<VendaResponse>(getResult.Content.ReadAsStringAsync().Result);

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, putResult.StatusCode);
        Assert.NotEmpty(putResponse.Erros);
        Assert.Equal(deStatus.ToString(), getResponse.VendaStatus);
    }
}
